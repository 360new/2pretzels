<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout.get');

Route::group(['prefix' => 'admin', 'middleware' =>['auth', 'check.role']], function () {

    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::any('images/upload', 'ImageController@ajaxUpload')->name('image.upload');
    Route::get('generate_slug', 'ServiceController@generateSlug')->name('generate.slug');
    Route::post('set_tag', 'ServiceController@setTag')->name('set.tag');
    Route::get('delete-all-products', 'ServiceController@deleteAllProducts')->name('delete-all-products');

    Route::get('users/{id}/restore', 'UserController@restore')->name('users.restore');
    Route::get('users/{id}/delete', 'UserController@destroy')->name('users.delete');
    Route::resource('users', 'UserController');

    Route::get('categories/{id}/restore', 'CategoryController@restore')->name('categories.restore');
    Route::get('categories/{id}/delete', 'CategoryController@destroy')->name('categories.delete');
    Route::resource('categories', 'CategoryController');

    Route::get('posts/{id}/restore', 'PostController@restore')->name('posts.restore');
    Route::get('posts/{id}/delete', 'PostController@destroy')->name('posts.delete');
    Route::resource('posts', 'PostController');

    Route::get('/settings', 'SettingsController@index')->name('settings.index');
    Route::post('/settings', 'SettingsController@update')->name('settings.update');

    Route::get('/menus', 'MenuController@index')->name('menus.index');
    Route::post('/menus/{id}', 'MenuController@update')->name('menus.update');



    Route::group(['namespace' => 'Shop'], function () {

        Route::get('orders/{id}/restore', 'OrderController@restore')->name('orders.restore');
        Route::get('orders/{id}/delete', 'OrderController@destroy')->name('orders.delete');
        Route::post('orders/update_status', 'OrderController@updateStatus')->name('orders.update.status');
        Route::resource('orders', 'OrderController');

        Route::post('products/change_status', 'ProductController@changeStatus')->name('products.status-change');
        Route::post('products/change_sold', 'ProductController@changeSold')->name('products.sold-change');
        Route::post('products/change_top', 'ProductController@changeTop')->name('products.top-change');
        Route::get('products/{id}/restore', 'ProductController@restore')->name('products.restore');
        Route::get('products/{id}/delete', 'ProductController@destroy')->name('products.delete');
        Route::resource('products', 'ProductController');

        Route::get('product_categories/{id}/restore', 'ProductCategoryController@restore')->name('product_categories.restore');
        Route::get('product_categories/{id}/delete', 'ProductCategoryController@destroy')->name('product_categories.delete');
        Route::resource('product_categories', 'ProductCategoryController');

    });


});

Route::get('/','FrontController@getHomepage')->name('homepage');
Route::get('/cart','FrontController@getCart')->name('cart');
Route::get('/account','FrontController@getCabinet')->name('cabinet')->middleware('auth');
Route::post('/create_cart', 'Shop\OrderController@createCartOrder')->name('cart.create');
Route::post('/get_cart', 'Shop\OrderController@getCartOrder')->name('cart.get');
Route::get('/get_cart_products', 'Shop\OrderController@getCartProducts')->name('cart.get.products');
Route::post('/update_cart_items', 'Shop\OrderController@updateOrderItems')->name('cart.change.products');
Route::post('/confirm_order', 'Shop\OrderController@confirmOrder')->name('cart.confirm');
Route::get('/get_products', 'Shop\ProductController@getProductsJSON')->name('products.get');
Route::get('/pages/{pages?}','FrontController@getPage')->where('pages', '.*')->name('page');
Route::get('/catalog/tags/{slug}','FrontController@getTag')->name('product.tags');
Route::get('/catalog/{products?}','FrontController@getShopPage')->where('products', '.*')->name('products.categories');


//Custom routes
Route::post('/get_category_products', 'FrontController@getCategoryProducts')->name('get.category.products');
Route::post('/get_top_products', 'FrontController@getTopProducts')->name('get.top.products');

