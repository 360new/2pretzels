(function ($) {
    // USE STRICT
    "use strict";
    // let offset = (document.documentElement.clientWidth>1199)?120:0;
    $(document).ready(function () {
        /* initialize the slider based on the Slider's ID attribute from the wrapper above */
        $('#rev_slider').show().revolution({
            /* options are 'auto', 'fullwidth' or 'fullscreen' */
            responsiveLevels: [1201, 1200, 992, 768],
            gridwidth: [1170, 970, 740, 700],
            sliderLayout: 'fullwidth',
            minHeight: '500',
            delay: 4000,
            /* basic navigation arrows and bullets */
            navigation: {

                arrows: {
                    enable: true,
                    style: 'gyges',
                    hide_onleave: false,
                    left: {
                        h_offset: 0
                    },
                    right: {
                        h_offset: 0
                    }
                },

                bullets: {
                    enable: true,
                    style: 'hermes',
                    hide_onleave: false,
                    h_align: "center",
                    v_align: "bottom",
                    h_offset: 0,
                    v_offset: 20,
                    space: 5
                }
            }
        });

        /* initialize the slider based on the Slider's ID attribute from the wrapper above */
        $('#rev_slider_2').show().revolution({
            /* options are 'auto', 'fullwidth' or 'fullscreen' */
            sliderLayout: 'auto',
            minHeight: 845,
            gridheight: 845,
            responsiveLevels: [1201, 1200, 992, 768],
            gridwidth: [1170, 970, 740, 700],
            delay: 2500,
            /* basic navigation arrows and bullets */
            navigation: {

                arrows: {
                    enable: false
                },

                bullets: {
                    enable: true,
                    style: 'hermes',
                    hide_onleave: false,
                    h_align: "center",
                    v_align: "bottom",
                    h_offset: 0,
                    v_offset: 20,
                    space: 5
                }
            }
        });
    });

})(jQuery);

