//cart controls
$('body').on('click', '.dish-control-plus', function() {
    let suk = $(this).prev().val();
    let prodmin = (!!!$(this).prev().data('prodmin'))?0:$(this).prev().data('prodmin');
    if (prodmin==0){
        $(this).prev().val( (suk=='') ?0:(suk ==999)? 999: Number(suk) + 1 );
    }
    else
    {
        $(this).prev().val((suk == '') ? 0 : (suk == 999) ? 999 : (suk == 0) ? prodmin : Number(suk) + 1);
    }
    inputpassmin($(this).prev());
});

$('body').on('click', '.dish-control-minus', function() {
    let suk = $(this).next().val();
    let prodmin = (!!!$(this).next().data('prodmin'))?0:$(this).next().data('prodmin');
    if (prodmin==0){
        $(this).next().val( (suk=='' || suk <1) ? 0: Number(suk) - 1 );
    }
    else
    {
        $(this).next().val( (suk=='' || suk <+prodmin+1) ? 0: Number(suk) - 1 );
    }
    inputpassmin($(this).next());
});

function strToNum(str){
    return +str.replace(/\D+/g,"");
}

$('body').on('focus', '.dish-control-input', function() {
    // $('.dish-control-input').focus(function () {
    $(this).data('oldval',$(this).val());
});
$('body').on('input propertychange', '.dish-control-input', function() {
    inputpassmin(this);
});

function inputpassmin(input){
    // console.log('lkl'+$(input).val());
    if ($(input).data('oldval')>$(input).val()){
        //console.log('tomin');
        if(!!$(input).data('prodmin')&&$(input).val()>0&&$(input).val()<$(input).data('prodmin')){
            $(input).val(0);
        }
    }else{
        //console.log('tomax');
        if(!!$(input).data('prodmin')&&$(input).val()>0&&$(input).val()<$(input).data('prodmin')){
            $(input).val($(input).data('prodmin'));
        }
    }
    let suk = $(input).val();
    if(suk.length>3) {
        suk=suk.slice(1,4);
        $(input).val(strToNum(suk));
    }
    $(input).data('oldval',$(input).val());
}

$(document).ready( function () {
    if( Cookies.get('order_id') === undefined) {
        console.log('Заказ не найден');
        $.post('/create_cart', {_token: $('.csrf-field').val()}, function (data) {
            Cookies.set('order_id', data);
        });
    }
    getCart();
});


$('.set-cart').click(function(){
    setCart(this);
});

$('body').on('click', '.dish-control-cart2', function() {
    var item, id, quantity;
    item = $(this).closest('.menu-item');
    id = $(item).data('product-id');
    quantity = $(item).find('.dish-control-input').val();
    console.log(id+' '+ quantity+' '+$(item)[0].className);
    setCart(id,quantity==''?0:quantity);

    if(quantity == 0) {
        item.fadeOut(500)
    }
});


$('body').on('click', '.dish-control-cart', function() {

    var item, id, quantity;
    item = $(this).closest('.menu-item');
    id = $(item).data('product-id');
    quantity = $(item).find('.dish-control-input').val();
    // console.log(quantity+' '+id+' '+$(item)[0].className);
    setCart(id,quantity==''?0:quantity);

    if(quantity == 0) {
        item.remove();
        $('.menu-list-summary-summ').html(0);
    }
});

$('body').on('click', '.dish-control-del', function() {
    var item, id, quantity;
    item = $(this).closest('.product-item');
    id = $(item).data('product-id');
    $(item).find('.dish-control-input').val(0);
    // console.log(quantity+' '+id+' '+$(item)[0].className);
    if (typeof gdata != "undefined") {
        var catdelid = $('.slytablink.active>span').hasClass('cattop')?'top':$('.slytablink.active>span')[0].className.replace(/\D+/g,"");
        for (var searchid in gdata[catdelid]){
            if(gdata[catdelid][searchid].id==id){
                gdata[catdelid][searchid].quantity=0;
            };
        }
    }
    setCart(id,0);
    $('.headroom').removeClass('headroom--unpinned').addClass('headroom--pinned');
    item.removeClass('cartin');
    if ($('.cart-menu').length>0) item.remove();
});

var cart,cartplace;
var imgwidth,imgheight;
cartitem();

function cartitem() {
    if (document.documentElement.clientWidth<768) {
        imgwidth='80%';
        imgheight='auto';
        cart = $('.shopping-cart-mobile');
        cartplace=80;
    }else if (document.documentElement.clientWidth<992){
        imgwidth='80%';
        imgheight='auto';
        cart = $('.shopping-cart-mobile');
        cartplace=80;
    }else{
        imgwidth='20%';
        imgheight='auto';
        cart = $('.dropcart');
        cartplace=110;
    }
}

$(window).on("resize",function(){
    cartitem();
});

$('body').on('click', '.dish-control-cart3', function() {
    var item, id, quantity;
    item = $(this).closest('.menu-item');
    id = $(item).data('product-id');
    quantity = $(item).find('.dish-control-input').val();
    // console.log(quantity+' '+id+' '+$(item)[0].className);
    setCart(id,quantity==''?0:quantity);
    //animate
    var imgtodrag = item.find("img").eq(0);
    // console.log(imgtodrag);
    dragImg(imgtodrag,quantity);
});

function dragImg(imgtodrag,quantity) {
    if (imgtodrag&&quantity>0) {
        var carttopfix = ($('.headroom').hasClass('headroom--unpinned'))?cartplace:0;
        $('.headroom').removeClass('headroom--unpinned').addClass('headroom--pinned');
        // var imgwidth,imgheight;
        var imgclone = imgtodrag.clone()
            .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
            .css({
                'opacity': '0.5',
                'position': 'absolute',
                'height': imgheight,
                'width': imgwidth,
                'z-index': '10002'
            })
            .appendTo($('body'))
            .animate({
                'top': cart.offset().top+carttopfix+20,
                'left': cart.offset().left+10,
                'width': 20,
                'height':20
            }, 1000, 'easeInOutExpo');
        imgclone.animate({
            'width': 0,
            'height': 0
        }, function () {
            $(this).detach()
        });
    }
}

$('body').on('click', '.dish-control-cart4', function() {
    var item, id, quantity;
    item = $(this).closest('.product-item');
    id = $(item).data('product-id');
    quantity = $(item).find('.dish-control-input').val();
    // console.log(quantity+' '+id+' '+$(item)[0].className);
    setCart(id,quantity==''?0:quantity);
    var imgtodrag = item.find("img").eq(0);
    // console.log(imgtodrag);
    dragImg(imgtodrag,quantity);
});

function getCart() {
    $.post('/get_cart', {_token: $('.csrf-field').val(), order_id: Cookies.get('order_id') }, function (data) {
        console.log(data);
        $('.cart-items-badge').text(data.quantity);
        if (data.quantity===0) {
            $('.cart-items-badge').addClass('fade');
            $('.shopping-cart-mobile-undertext').text("0₽");
        } else {
            $('.cart-items-badge').removeClass('fade');
            $('.shopping-cart-mobile-undertext').text(data.summ + "₽");
        };
        $('.cart-items-summ').text(data.summ);
        if($('.menu-list-summary').length > 0) {
            $('.menu-list-summary-summ').text(data.summ);
        }
        if((+data.summ<=4000)&&$('.confirm-client-order').length>0){
            console.log('<4000!');
            $('.confirm-client-order').attr('disabled',true).removeClass('priceok');
            $('.menu-list-summary-info').text('Минимальная сумма заказа - 4000₽');
        }else{
            console.log('>4000');
            $('.confirm-client-order').addClass('priceok');
            $('.menu-list-summary-info').text('');
        }
    });
}

function setCart(id,quantity) {
    var product = {id: id, quantity: +quantity};
    console.log(product);
    $.post('/update_cart_items', {_token: $('.csrf-field').val(), order_id: Cookies.get('order_id'), product: product}, function (data) {
        getCart();

        if (quantity>0) {
            // $('body [data-product-id='+id+']>div:last-child>div:last-child').shake({distance:5,times:2,speed:100});
            console.log(cart);
            setTimeout(function () {
                cart.shake({distance:5,times:2,speed:100,direction:'up'});
            }, 1500);
            $('body [data-product-id='+id+']').addClass('cartin')}
            else {
            $('body [data-product-id='+id+']').removeClass('cartin');
            cart.shake({distance:5,times:2,speed:100});
        }
    });
}





(function($) {
    $.fn.shake = function(o) {
        if (typeof o === 'function')
            o = {callback: o};
        // Set options
        var o = $.extend({
            direction: "left",
            distance: 20,
            times: 3,
            speed: 140,
            easing: "swing"
        }, o);

        return this.each(function() {

            // Create element
            var el = $(this), props = {
                position: el.css("position"),
                top: el.css("top"),
                bottom: el.css("bottom"),
                left: el.css("left"),
                right: el.css("right")
            };

            el.css("position", "relative");

            // Adjust
            var ref = (o.direction == "up" || o.direction == "down") ? "top" : "left";
            var motion = (o.direction == "up" || o.direction == "left") ? "pos" : "neg";

            // Animation
            var animation = {}, animation1 = {}, animation2 = {};
            animation[ref] = (motion == "pos" ? "-=" : "+=")  + o.distance;
            animation1[ref] = (motion == "pos" ? "+=" : "-=")  + o.distance * 2;
            animation2[ref] = (motion == "pos" ? "-=" : "+=")  + o.distance * 2;

            // Animate
            el.animate(animation, o.speed, o.easing);
            for (var i = 1; i < o.times; i++) { // Shakes
                el.animate(animation1, o.speed, o.easing).animate(animation2, o.speed, o.easing);
            };
            el.animate(animation1, o.speed, o.easing).
            animate(animation, o.speed / 2, o.easing, function(){ // Last shake
                el.css(props); // Restore
                if(o.callback) o.callback.apply(this, arguments); // Callback
            });
        });
    };
})(jQuery);