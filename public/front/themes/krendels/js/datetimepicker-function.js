(function ($) {
    // USE STRICT
    "use strict";

    $(document).ready(function() {
        $('.au-select-date').datetimepicker({
            timepicker: false,
            format:'d/m/Y'
        });
        $('.au-select-time').datetimepicker({
            format:'d/m/Y H:i'
        });
    });
    $.datetimepicker.setLocale('ru');
})(jQuery);