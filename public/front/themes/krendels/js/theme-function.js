

(function ($) {
    // USE STRICT
    "use strict";
    console.log('custom in');
    $(document).ready(function() {
        var accordion_select = $('.accordion');
        accordion_select.accordion({
            "transitionSpeed": 400
        });
    });

})(jQuery);





(function ($) {
    // USE STRICT
    "use strict";

    $(document).ready(function() {
        $('.au-select-date').datetimepicker({
            timepicker: false,
            format:'d/m/Y'
        });
        $('.au-select-time').datetimepicker({
            // datepicker: false,
            format:'d/m/Y H:i'
        });
    });

})(jQuery);
/*jQuery*/

(function ($) {
    // USE STRICT
    "use strict";

    $(document).ready(function () {
        var ink, d, x, y;
        $(".ripplelink").on('click', function(e){
            if($(this).find(".ink").length === 0){
                $(this).prepend("<span class='ink'></span>");
            }

            ink = $(this).find(".ink");
            ink.removeClass("animate");

            if(!ink.height() && !ink.width()){
                d = Math.max($(this).outerWidth(), $(this).outerHeight());
                ink.css({height: d, width: d});
            }

            x = e.pageX - $(this).offset().left - ink.width()/2;
            y = e.pageY - $(this).offset().top - ink.height()/2;

            ink.css({top: y+'px', left: x+'px'}).addClass("animate");
        });

        // Look for .hamburger
        var hamburger = $(".hamburger");
        var navbar = $('.navbar-menu');
        // On click
        hamburger.on("click", function() {
            // Toggle class "is-active"
            hamburger.toggleClass("is-active");
            // Do something else, like open/close menu
            navbar.toggleClass('opened');
        });

        $('nav ul .dropdown i').on('click', function(e) {
            $(this).siblings('.dropdown-menu').toggleClass('opened');
            $(this).toggleClass('rotate-180');
            // Close one dropdown when selecting another
            //$('.dropdown-menu').not($(this).siblings()).removeClass('opened');
            e.stopPropagation();
        });

        $('nav ul .dropdown-child i').on('click', function(e) {
            $(this).siblings('.dropdown-menu-child').toggleClass('opened');
            $(this).toggleClass('rotate-180');
            // Close one dropdown when selecting another
            //$('.dropdown-menu').not($(this).siblings()).removeClass('opened');
            e.stopPropagation();
        });

        // Fixed navbar on scroll
        $(".header .header-inner").headroom();

    });

})(jQuery);


(function ($) {
    // USE STRICT
    "use strict";

    $(document).ready(function() {
        $('.matchHeight').matchHeight({});



    });

})(jQuery);
(function ($) {
    // USE STRICT
    "use strict";

    $(document).ready(function() {

        var select = $('.au-select');
        select.niceSelect();
    });

})(jQuery);





(function ($) {
    // USE STRICT
    "use strict";

    $(document).ready(function() {
        var owlSelector = $('.owl-carousel');
        owlSelector.each(function () {
            var owl = $(this);
            var dots = $(this).attr('data-carousel-dots');
            var center = false;
            var col_sm =  1;
            var col_md =  1;
            var col_lg =  1;
            var col_xs =  1;
            var items = 1;
            var animateOut = null;
            var animateIn = null;
            var marginSpace = 0;
            if ($(this).attr('data-carousel-margin') != null) {
                marginSpace = $(this).data('carousel-margin');
            }
            if ($(this).attr('data-carousel-out') != null) {
                animateOut = $(this).attr('data-carousel-out');
            }
            if ($(this).attr('data-carousel-in') != null) {
                animateIn = $(this).attr('data-carousel-in');
            }
            if ($(this).attr('data-carousel-center') == 1) {
                center = true;
            }
            if ($(this).attr('data-carousel-items') != null) {
                items = $(this).attr('data-carousel-items');
            }
            if ($(this).attr('data-carousel-xs') != null ||
                $(this).attr('data-carousel-sm') != null ||
                $(this).attr('data-carousel-lg') != null ||
                $(this).attr('data-carousel-md') != null) {

                col_sm = $(this).attr('data-carousel-sm');
                col_md = $(this).attr('data-carousel-md');
                col_lg = $(this).attr('data-carousel-lg');
                col_xs = $(this).attr('data-carousel-xs');
            }
            $(this).owlCarousel({
                autoplay: true,
                dotsContainer: dots,
                loop: true,
                animateOut: animateOut,
                animateIn: animateIn,
                margin: marginSpace,
                responsive: {
                    // breakpoint from 0 up
                    0: {
                        items: col_xs,
                        center:center
                    },
                    // breakpoint from 480 up
                    320: {
                        items: col_sm,
                        center:center
                    },
                    // breakpoint from 768 up
                    480: {
                        items: col_md,
                        center:center
                    },
                    992: {
                        items: col_lg,
                        center:center
                    },
                    1200: {
                        items : items,
                        center: center
                    }
                }
            });

            $('.owl-dots .owl-dot').on('click', function () {
                owl.trigger('to.owl.carousel', [$(this).index(), 300]);
            });

            // Go to the next item
            $('.owl-next').on('click', function() {
                owl.trigger('next.owl.carousel', [300]);
            });
            // Go to the previous item
            $('.owl-prev').on('click', function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                owl.trigger('prev.owl.carousel', [300]);
            });
        });
    });

})(jQuery);




