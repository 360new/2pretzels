/*jQuery*/

(function ($) {
    // USE STRICT
    "use strict";

    $(document).ready(function () {
        var ink, d, x, y;
        $(".ripplelink").on('click', function(e){
            if($(this).find(".ink").length === 0){
                $(this).prepend("<span class='ink'></span>");
            }

            ink = $(this).find(".ink");
            ink.removeClass("animate");

            if(!ink.height() && !ink.width()){
                d = Math.max($(this).outerWidth(), $(this).outerHeight());
                ink.css({height: d, width: d});
            }

            x = e.pageX - $(this).offset().left - ink.width()/2;
            y = e.pageY - $(this).offset().top - ink.height()/2;

            ink.css({top: y+'px', left: x+'px'}).addClass("animate");
        });

        // Look for .hamburger
        var hamburger = $(".hamburger");
        var navbar = $('.navbar-menu');
        var scrollme;
        // On click
        hamburger.on("click", function() {
            // Toggle class "is-active"
            hamburger.toggleClass("is-active");
            // Do something else, like open/close menu
            navbar.toggleClass('opened');
            if (!hamburger.hasClass('is-active')){
                $('.opened').removeClass('opened');
                $('body').removeClass('nav-opened');
                $('html, body').animate({
                    scrollTop: scrollme
                }, 100);
            }else{
                scrollme=$('html, body').scrollTop();

            }
        });

        $('nav ul .dropdown i').on('click', function(e) {
            $(this).siblings('.dropdown-menu').toggleClass('opened');
            $(this).toggleClass('rotate-180');
            // Close one dropdown when selecting another
            //$('.dropdown-menu').not($(this).siblings()).removeClass('opened');
            e.stopPropagation();
        });
        $('nav ul .dropdown-child i').on('click', function(e) {
            $(this).siblings('.dropdown-menu-child').toggleClass('opened');
            $(this).toggleClass('rotate-180');
            // Close one dropdown when selecting another
            //$('.dropdown-menu').not($(this).siblings()).removeClass('opened');
            e.stopPropagation();
        });


        //header dropdowns
        $('.droplogin-text').on('click', function(e) {
            $(this).siblings('.dropdown-menu').toggleClass('opened');
            if($('.opened').length>1) {
                $('body').addClass('nav-opened');
            } else{
                $('body').removeClass('nav-opened');
            }
            $(this).parents('.dropdown').find('i').toggleClass('rotate-180');
            // Close one dropdown when selecting another
            //$('.dropdown-menu').not($(this).siblings()).removeClass('opened');
            e.stopPropagation();
        });

        // $(this).parents('.dropdown').find('i').toggleClass('rotate-180');

        $('.dropcart-text').on('click', function(e) {
            $(this).siblings('.dropdown-menu').toggleClass('opened');
            if($('.opened').length>1) {
                $('body').addClass('nav-opened');
            } else{
                $('body').removeClass('nav-opened');
            }
            $(this).parents('.dropdown').find('i').toggleClass('rotate-180');
            // Close one dropdown when selecting another
            //$('.dropdown-menu').not($(this).siblings()).removeClass('opened');
            e.stopPropagation();
        });

        $('.dropdown-shopping').on('click', function(e) {
            $(this).siblings('.dropdown-menu').toggleClass('opened');
            if($('.opened').length>1) {
                $('body').addClass('nav-opened');
            } else{
                $('body').removeClass('nav-opened');
            }
            $(this).parents('.dropdown').find('i').toggleClass('rotate-180');
            // Close one dropdown when selecting another
            //$('.dropdown-menu').not($(this).siblings()).removeClass('opened');
            e.stopPropagation();
        });

        $('body').on('click','.dropdown-caption', function(e) {
            $(this).siblings('.dropdown-menu').toggleClass('opened');
            if($('.opened').length>1) {
                $('body').addClass('nav-opened');
            } else{
                $('body').removeClass('nav-opened');
            }
            $(this).parents('.dropdown').find('i').toggleClass('rotate-180');
            // Close one dropdown when selecting another
            //$('.dropdown-menu').not($(this).siblings()).removeClass('opened');
            e.stopPropagation();
        });

        // Fixed navbar on scroll
        $(".header .header-inner").headroom();

    });

})(jQuery);


function imgerror(img) {
    img.src='/front/themes/krendels/img/placeholder.jpg';
}


