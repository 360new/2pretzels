//SLY control
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $(".csrf-field").val()
    }
});

var options = {
    horizontal: 1,
    itemNav: 'basic',
    speed: 300,
    scrollBar: $('#scrolltab'),
    // scrollBy: 1,
    mouseDragging: 1,
    touchDragging: 1,
    dragHandle: 1,
    dynamicHandle: 1,
    clickBar: 1,
    // scrollHijack: 500,
    // scrollTrap:   true,
    nextPage:  '.sly-arrow-right',
    prevPage:'.sly-arrow-left'
};
var sly,gdata= {},iteminwrap;
if (document.documentElement.clientWidth>992) {iteminwrap=3;} else {iteminwrap=2;}

$.ajax({
    url: "/get_top_products",
    method:"POST",
    beforeSend:function () {
        $('#mainslyframe').addClass('loading');
    },
    success: function(data){
        // console.log( "Прибыли данные: ");
        // console.log(JSON.parse(data));
        gdata['top'] = JSON.parse(data);
        sly =  new Sly('#mainslyframe', options).init()
        addcatmenu(gdata['top'],'top');
        $('#mainslyframe').removeClass('loading');
    }
});


$(window).on("resize",function(){
    sly.reload(); console.log('resized');
});

$('.slycat').click(function () {
    // $(this).parent().addClass('active');

    var id = $(this).hasClass('cattop')?'top':this.className.replace(/\D+/g,"");
    if (!gdata.hasOwnProperty(id)) {
        $.ajax({
            url: "/get_category_products?id=" + id,
            method: "POST",
            beforeSend:function () {
                $('#mainslyframe').addClass('loading');
            },
            success: function (data) {
                //console.log("Прибыли данные: ");
                gdata[id] = JSON.parse(data);
                addcatmenu(gdata[id], id);
                $('#mainslyframe').removeClass('loading');
            }
        });
    }else{
        addcatmenu(gdata[id], id);
    }
});

function addcatmenu(data,id) {
    if ($('.menu-item-wrap').length>0) {
        $('.menu-item-wrap').each(function () {
            sly.remove('.menu-item-wrap');
        });
    }
    var items = genmenu(data,id);
    // console.log(items);
    for (var i = 0; i < items.length; i++) {
        //console.log(items[i]);
        sly.add(items[i]);
    }
}

function genmenu(data,catid) {
    //ajax давай здесь вставим
    var elemnum=data.length;
    //console.log('elems = ' + elemnum);
    var wraps = [],item,wrapitem;
    var wrapsnum = Math.ceil(elemnum/iteminwrap);
    //console.log('wrappas = '+wrapsnum);
    //console.log(data);
    catid = (catid=='top')?'cattop':'cat'+catid;
    for (var t = 0; t < wrapsnum; t++) {
        wrapitem='<li class="menu-item-wrap '+catid+'">\n';
        for (var i = t*iteminwrap; i < t*iteminwrap+iteminwrap; i++){
            if(!!!data[i]) {console.log('undef');continue;}
            // console.log('i'+ i +' nom '+ data[i].id+' is '+!!data[i]+' th '+data[i].hasOwnProperty('images'));
            var imagesrc = (data[i].images[0]!==undefined)?data[i].images[0].medium+'?v=1':'/front/themes/krendels/img/placeholder.jpg';
            var soldtext = (data[i])?'':'Товар недоступен';
            var cartin = (data[i].quantity>0)?'cartin':'';
            wrapitem+='<div class="menu-item product-item p-t-15 p-b-15 clearfix '+cartin+'" data-product-id="'+data[i].id+'">\n' +
                '                                        <div class="dish-main clearfix">\n' +
                '                                            <a href="'+data[i].category_link + '/' + data[i].slug + '" class="dish-image">\n' +
                '                                                \n' +
                '                                                   <img src="'+imagesrc+'" alt="" onerror="imgerror(this)">\n' +
                '                                            </a>\n' +
                '                                            <div class="dish-content">\n' +
                '                                                <div class="dish-name text-uppercase">\n' +
                '                                                    <a href="'+data[i].category_link+'/'+data[i].slug+'" class="heading-font">'+data[i].name+'</a>\n' +
                '                                                </div>\n' +
                '                                                <div class="dish-topic">\n' +
                '                                                    <a href="'+data[i].category_link+'">'+data[i].category_name+'</a>\n' +
                '                                                </div>\n' +
                '                                            </div>\n' +
                '                                            <div class="dish-cost">\n' +
                '                                               <div class="dish-cost-ves"><span class="dish-control-ves-num">'+data[i].weight+'</span> <span class="dish-control-ves-count"></span></div>\n' +
                '                                                <span>'+data[i].price+' <span class="ruble">₽</span></span>\n' +
                '                                            </div>\n' +
                '\n' +
                '                                        </div>\n' +
                '\n' +
                '                                        <div class="dish-control dish-control-sold'+data[i].sold+'">\n' +
                                                            '<div class="dish-control-del"></div>\n'+
                '                                            <div class="dish-control-empty">'+soldtext+'</div>\n'  +
                '                                            <div class="dish-control-minus"></div>\n' +
                '                                            <input class="dish-control-input" maxlength="3" max="999" min="0" data-prodmin="'+data[i].minimum+'" type="number" size="3" value="' + data[i].quantity + '">\n' +
                '                                            <div class="dish-control-plus"></div>\n' +
                '                                            <div class="dish-control-cart3"></div>\n' +
                '\n' +
                '                                        </div>\n' +
                '\n' +
                '                                    </div>\n';
        }
        wrapitem+='</li>';
        wraps.push(wrapitem);
    }
    // console.log(wraps);
    return wraps;
}
