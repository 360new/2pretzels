
    //data-map-x="55.7971154" data-map-y="37.5353427" data-map-x="56.7971154" data-map-y="38.5353427"
    $(document).ready(function () {
        var dark = [{
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [{"saturation": 36}, {"color": "#000000"}, {"lightness": 40}]
        }, {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [{"visibility": "on"}, {"color": "#000000"}, {"lightness": 16}]
        }, {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [{"visibility": "off"}]
        }, {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#000000"}, {"lightness": 20}]
        }, {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#000000"}, {"lightness": 17}, {"weight": 1.2}]
        }, {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [{"color": "#000000"}, {"lightness": 20}]
        }, {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [{"color": "#000000"}, {"lightness": 21}]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#000000"}, {"lightness": 17}]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#000000"}, {"lightness": 29}, {"weight": 0.2}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [{"color": "#000000"}, {"lightness": 18}]
        }, {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [{"color": "#000000"}, {"lightness": 16}]
        }, {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [{"color": "#000000"}, {"lightness": 19}]
        }, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#000000"}, {"lightness": 17}]}];

        var iconcustom = new google.maps.MarkerImage("/front/themes/krendels/img/icons/ic-cricle-a.png", new google.maps.Size(40, 40),
            new google.maps.Point(0, 0), new google.maps.Point(20, 55));
        var map = new GMaps({
            div: '#google_map',
            lat: 55.762359,
            lng: 37.618369,
            zoom:11,
            styles: dark
        });
        //marker1
        map.addMarker({
            lat: 55.7971124,
            lng: 37.5375314,
            icon:iconcustom,
            infoWindow: {
                content: '<h4>Два кренделя</h4><b>Email: info@2krendelya.ru <br>Тел.: +7 (495) 123-45-67</p><a href="#">м. Аэропорт, Ленинградский пр., д. 39, стр. 80</a>'
            },
            onclick: function() {
                this.infoWindow.open(this.map, this);
            }
            // ,
            // mouseout: function() {
            //     // this.infoWindow.close();
            // }
        });
        //marker2
        map.addMarker({
            lat: 55.7694185,
            lng: 37.6492375,
            icon:iconcustom,
            infoWindow: {
                content: '<h4>Два кренделя</h4><b>Email: info@2krendelya.ru <br>Тел.: +7 (495) 123-45-67</p><a href="#">м. Красные ворота, Орликов переулок, д. 5</a>'
            },
            onclick: function() {
                this.infoWindow.open(this.map, this);
            }
            // ,
            // mouseout: function() {
            //     // this.infoWindow.close();
            // }
        });
        //marker3
        map.addMarker({
            lat: 55.692308,
            lng: 37.662785,
            icon:iconcustom,
            infoWindow: {
                content: '<h4>Два кренделя</h4><b>Email: info@2krendelya.ru <br>Тел.: +7 (495) 123-45-67</p><a href="#">м. Технопарк, пр. Андропова, д.18, корп. 5</a>'
            },
            onclick: function() {
                this.infoWindow.open(this.map, this);
            }
            // ,
            // mouseout: function() {
            //     // this.infoWindow.close();
            // }
        });
        //marker4
        map.addMarker({
            lat: 55.769707,
            lng: 37.625046,
            icon:iconcustom,
            infoWindow: {
                content: '<h4>Два кренделя</h4><b>Email: info@2krendelya.ru <br>Тел.: +7 (495) 123-45-67</p><a href="#">м. Трубная, ул. Трубная, д.17, стр. 1</a>'
            },
            onclick: function() {
                this.infoWindow.open(this.map, this);
            }
            // ,
            // mouseout: function() {
            //     // this.infoWindow.close();
            // }
        });
        //marker5
        map.addMarker({
            lat: 55.733842,
            lng: 37.588144,
            icon:iconcustom,
            infoWindow: {
                content: '<h4>Два кренделя</h4><b>Email: info@2krendelya.ru <br>Тел.: +7 (495) 123-45-67</p><a href="#">м. Парк культуры, ул. Льва Толстого, д. 16</a>'
            },
            onclick: function() {
                this.infoWindow.open(this.map, this);
            }
            // ,
            // mouseout: function() {
            //     // this.infoWindow.close();
            // }
        });
        //marker6
        map.addMarker({
            lat: 55.743892,
            lng: 37.656586,
            icon:iconcustom,
            infoWindow: {
                content: '<h4>Два кренделя</h4><b>Email: info@2krendelya.ru <br>Тел.: +7 (495) 123-45-67</p><a href="#">м. Таганская, Б. Дровяной переулок, д. 8, стр. 2</a>'
            },
            onclick: function() {
                this.infoWindow.open(this.map, this);
            }
            // ,
            // mouseout: function() {
            //     // this.infoWindow.close();
            // }
        });
    });
