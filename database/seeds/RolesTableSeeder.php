<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'role' => 'admin',
            'role_name' => 'Администратор'
        ]);

        DB::table('roles')->insert([
            'role' => 'moderator',
            'role_name' => 'Модератор'
        ]);

        DB::table('roles')->insert([
            'role' => 'user',
            'role_name' => 'Пользователь'
        ]);

        DB::table('roles')->insert([
            'role' => 'shop_manager',
            'role_name' => 'Администратор магазина'
        ]);
        $this->call(UsersTableSeeder::class);
    }
}
