<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('settings')->insert([
            'contacts_email' => 'dev.vladimirkarpenko@gmail.com',
            'site_name' => 'Новый сайт',
            'default_keywords'=> 'новый, сайт',
            'default_description' => 'Новый сайт'
        ]);
    }
}
