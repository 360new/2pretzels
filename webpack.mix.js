let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
.options({
    postCss: [require('autoprefixer')]
})
// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');
.browserSync({
    files: [
        'app/**/*',
        'public/**/*',
        'resources/views/**/*',
        'resources/lang/**/*',
        'routes/**/*'
    ],
    injectFileTypes: ["css", "png", "jpg", "jpeg", "svg", "gif", "webp"],
    injectChanges: true,
    proxy: 'localhost:8000',
    notify: {
        styles: {
            top: 'auto',
            bottom: '0'
        }
    },
})
.standaloneSass('resources/assets/sass/main.scss', 'public/css');