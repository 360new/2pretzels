<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function parent()
    {
        return $this->morphTo();
    }

    public function deleteWithFiles()
    {
        if(file_exists(public_path($this->thumbnail))) {
            unlink(public_path($this->thumbnail));
        }

        if(file_exists(public_path($this->small))) {
            unlink(public_path($this->small));
        }

        if(file_exists(public_path($this->medium))) {
            unlink(public_path($this->medium));
        }

        if(file_exists(public_path($this->large))) {
            unlink(public_path($this->large));
        }
        $this->delete();
    }
}
