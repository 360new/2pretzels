<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use SoftDeletes;

    protected $guarded = ['sales','categories', 'image', 'form_type','gallery','img_delete'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Shop\ProductCategory','product_category_product');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'parent');
    }

    public function miniature()
    {
        return $this->images()->where('type', 'miniature')->first();
    }

    public function gallery()
    {
        return $this->images()->where('type', 'gallery')->get();
    }
    public function getReadableStatus()
    {
        $status = null;
        if (!is_null($this->deleted_at)) {
            $status = 'В корзине';
        } else {
            switch ($this->status) {
                case 'hidden':
                    $status = 'Черновик';
                    break;
                case 'published':
                    $status = 'Опубликовано';
                    break;
            }
        }
        return $status;
    }
}
