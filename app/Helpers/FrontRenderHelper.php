<?php

namespace App\Helpers;

use App\Models\Post;

use App\Models\Shop\ProductCategory;
use Illuminate\Http\Request;

class FrontRenderHelper {

    public static function getCategoryPosts($category, $quantity)
    {
        return Post::whereHas('categories', function($query) use ($category) {
            $query->where('slug', 'LIKE', "$category");

        })->take($quantity)->get();
    }

    public static function renderProductCategoriesMenu($parent_id = null, $title = "Каталог", $url = "#!") {

        $product_categories = ProductCategory::where('parent_id', $parent_id)->get();
        echo "<li class=\"dropdown\">";
        echo "<a href=\"". $url. "\">{$title}</a>";
        echo "<i class=\"fa fa-angle-down\"></i>";
        echo "<ul class=\"dropdown-menu\">";

        foreach ($product_categories as $category) {
            if($category->children->isEmpty()) {
                echo "<li>";
                echo "<a href=\"".$category->getUrl()."\">".$category->name."</a>";
                echo"</li>";
            } else {
                self::renderProductCategoriesMenu($category->id, $category->name, $category->getUrl());
            }
        }
        echo "</ul>";
        echo "</li>";
    }
}