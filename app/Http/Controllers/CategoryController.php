<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('trashed')) {
            $category = Category::onlyTrashed();
        } else {
            $category = Category::whereNotNull('user_id');
        }

        $category->where('type', $request->type);

        if(isset($request->status)) {
            $category->where('status', $request->status);
        }

        if(isset($request->user_id)) {
            $category->where('user_id', $request->user_id);
        }

        $category->where('parent_id', null);

        return view('admin.pages.categories.list',[
            'categories' => $category->get(),
            'title' => ' | Список категорий',
            'active' => 'Категории'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request['user_id'] = Auth::user()->id;
        $category = new Category($request->all());
        $category->id = 0;
        $category->type = $request->type;
        return view('admin.pages.categories.form',[
            'category' => $category,
            'title' => '| Создание категории',
            'categories' => Category::whereNotIn('id', [$category->id])->where('name', '<>', null)->get(),
            'active' => 'Создание'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->type = $request->type;
        $category->user_id = Auth::user()->id;
        $category->save();
        return Response($category->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::withTrashed()->where('id', $id)->first();

        return view('admin.pages.categories.form',[
            'category' => $category,
            'title' => '| Редактирование категории | '. $category->name,
            'categories' => Category::whereNotIn('id', [$id])->where('name', '<>', null)->get(),
            'active' => $category->name ? $category->name : 'Новая категория'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::withTrashed()->where('id', $id)->first();
        $category->fill($request->all());

        if(isset($request->image)){
            if($category->miniature()) {
                ImageController::deleteImage($category->miniature()->id);
            }
            ImageController::uploadImage( 'miniature', 'category', $id, $request->image);
        }

        if(isset($request->gallery)){
            ImageController::massiveUpload($request, 'gallery', 'gallery','category', $id);
        }

        if(isset($request->img_delete)){
            ImageController::massiveDelete($request->img_delete);
        }

        $category->save();

        return redirect(route('categories.edit', $category->id));
    }

    public function destroy($id)
    {
        $category = Category::withTrashed()->where('id', $id)->first();
        $type = $category->type;
        if ($category->trashed()) {
            $category->forceDelete();
            return redirect(route('categories.index', ['type' => $type]));
        } else {
            $category->delete();
            return Redirect::back();
        }
    }

    /**
     * Restore deleted Category.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $category = Category::withTrashed()->where('id', $id)->first();
        $category->restore();
        return Redirect::back();
    }
}
