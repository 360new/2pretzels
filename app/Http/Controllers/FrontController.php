<?php

namespace App\Http\Controllers;

use App\Helpers\FrontRenderHelper;
use App\Models\Post;
use App\Models\Settings;
use App\Models\Shop\Order;
use App\Models\Shop\Product;
use App\Models\Category;
use App\Models\Shop\ProductCategory;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Response;

class FrontController extends Controller
{

    public function __construct()
    {

    }

    public function getHomepage(Request $request)
    {

        /**
         * Получаем данные для подстановки в инпуты товаров
         */
        $cart = $cookie = null;
        $order = isset($_COOKIE['order_id']) ? Order::find($_COOKIE['order_id']) : new Order();
        if (is_null($order)) {
            $cookie = Cookie::forget('order_id');

        } else {
            $order_content = json_decode($order->content);
            if (!is_null($order_content)) {
                foreach ($order_content as $prod) {
                    $cart[$prod->id] = $prod->quantity;
                }
            }
        }
        $tabs = ProductCategory::where('status','public')->where('type', 'category')->whereIn('slug', ['zavtraki', 'salaty-i-zakuski', 'supy',
            'myaso', 'ptitsa', 'ryba', 'ovoshchi', 'khleb', 'pirozhki', 'pirogi', 'kruassany', 'deserty', 'napitki', 'garniry',
            'furshet-zakuski','furshet-goryachie-blyuda','furshet-deserty'])->get();
        $response = view('themes.' . env('THEME') . '.pages.home', [
            'cart' => $cart,
            'slider' => true,
            'top' => Product::where('status', 'public')->where('top', 1)->get(),
            'tabs' => $tabs,
            'FrontRenderHelper' => new FrontRenderHelper()
        ]);

        if (is_null($cookie)) {
            return $response;
        } else {
            return response($response)->withCookie($cookie);
        }

    }

    public function getPage(Request $request, $parameters = null)
    {
        $params = explode('/', $parameters);
        $content_slug = $params[count($params) - 1];

        /**
         * Определяем: выводится запись или категория
         */
        if (is_null($parameters)) {
            //Корень
            return Response::error(404);

        } elseif ($post = post::where('slug', $content_slug)->where('status', 'public')->first()) {
            //Страница

        } elseif ($post = Category::where('slug', $content_slug)->where('status', 'public')->firstOrFail()) {
            //Категория
            return Response::error(404);

        }

        return view('themes.' . env('THEME') . '.pages.page', [
            'post' => $post,
            'title' => $post->title . ' | ' . Settings::first()->site_name,
            'keywords' => $post->keywords,
            'description' => $post->description
        ]);
    }

    public function getCabinet()
    {

        return view('themes.' . env('THEME') . '.pages.cabinet', [
            'title' => 'Личный кабинет | ' . Settings::first()->site_name
        ]);
    }

    public function getCart(Request $request)
    {

        $order = isset($_COOKIE['order_id']) ? Order::find($_COOKIE['order_id']) : new Order();

        return view('themes.' . env('THEME') . '.pages.shop.cart', [
            'order_items' => $order->getOrderItems(),
            'title' => 'Корзина | ' . Settings::first()->site_name
        ]);
    }

    public function getTag(Request $request, $slug)
    {

        $cookie = null;
        $content = ProductCategory::where('type', 'tag')->where('slug', $slug)->where('status', 'public')->first();

        $products = Product::where('status', 'public')->WhereHas('categories', function ($query) use ($content) {
            $query->where('slug', 'LIKE', $content->slug);
        })->paginate(15);
        /**
         * Получаем данные для подстановки в инпуты товаров
         */
        $cart = null;
        $order = isset($_COOKIE['order_id']) ? Order::find($_COOKIE['order_id']) : new Order();
        if (is_null($order)) {
            $cookie = Cookie::forget('order_id');

        } else {
            $order_content = json_decode($order->content);

            if (!is_null($order_content)) {
                foreach ($order_content as $prod) {
                    $cart[$prod->id] = $prod->quantity;
                }
            }
        }

        //Оформление страницы
        $elements = [['url' => '/catalog', 'caption' => 'Каталог'], ['url' => '#!', 'caption' => $content->name]];
        $background = '/front/themes/krendels/img/_bg3.jpg';
        $tags = ProductCategory::where('type', 'tag')->where('status', 'public')->get();

        $response = view('themes.' . env('THEME') . '.pages.shop.category', [
            'content' => $content,
            'products' => $products,
            'cart' => $cart,
            'tags' => $tags,
            'background' => $background,
            'elements' => $elements,
            'title' => $content->name . ' | ' . Settings::first()->site_name,
            'keywords' => $content->keywords,
            'description' => $content->description
        ]);
        if (is_null($cookie)) {
            return $response;
        } else {
            return response($response)->withCookie($cookie);
        }

    }

    public function getShopPage(Request $request, $categories = null)
    {
        $view = $background = $products = '';
        $cookie = null;

        $parameters = explode('/', $categories);
        $content_slug = $parameters[count($parameters) - 1];
        $elements = [['url' => '/catalog', 'caption' => 'Каталог']];
        $background = '/front/themes/krendels/img/_bg3.jpg';

        /**
         * Определяем: выводится товар или коллекция
         */
        if (is_null($categories)) {
            $content = ProductCategory::where('type', 'category')->where('status', 'public')->where('parent_id', null)->get();
            $content->name = "Категории блюд";
            $content->keywords = $content->description = null;
            $view = 'categories';

        } elseif (count($parameters) > 1 && $content = Product::where('status', 'public')->where('slug', $content_slug)->first()) {

            $view = 'product';

        } elseif ($content = ProductCategory::where('status','public')->where('slug', $content_slug)->firstOrFail()) {
            $products = Product::where('status', 'public')->WhereHas('categories', function ($query) use ($content) {
                $query->where('slug', 'LIKE', $content->slug);
            })->paginate(18);
            $view = 'category';

        }

        /**
         * Генерация хлебных крошек
         */
        for ($i = 0; $i <= count($parameters) - 2; $i++) {
            $category = ProductCategory::where('slug', $parameters[$i])->firstOrFail();
            $elements[] = ['url' => $category->getUrl(), 'caption' => $category->name];
        }

        /**
         * Получаем данные для подстановки в инпуты товаров
         */
        $cart = null;
        $order = isset($_COOKIE['order_id']) ? Order::find($_COOKIE['order_id']) : new Order();
        if (is_null($order)) {
            $cookie = Cookie::forget('order_id');

        } else {
            $order_content = json_decode($order->content);

            if (!is_null($order_content)) {
                foreach ($order_content as $prod) {
                    $cart[$prod->id] = $prod->quantity;
                }
            }
        }
        $tags = ProductCategory::where('type', 'tag')->where('status', 'public')->get();
        $response = view('themes.' . env('THEME') . '.pages.shop.' . $view, [
            'content' => $content,
            'products' => $products,
            'cart' => $cart,
            'tags' => $tags,
            'background' => $background,
            'elements' => $elements,
            'title' => $content->name . ' | ' . Settings::first()->site_name,
            'keywords' => $content->keywords,
            'description' => $content->description
        ]);
        if (is_null($cookie)) {
            return $response;
        } else {
            return response($response)->withCookie($cookie);
        }
    }

    public function getCategoryProducts(Request $request)
    {
        $products = Product::where('status', 'public')->WhereHas('categories', function ($query) use ($request) {
            $query->where('product_category_id', $request->id);
        })->with(['images' => function ($query) {
            $query->where('type', 'miniature');
        }])->get();

        $cart = [];
        $order = isset($_COOKIE['order_id']) ? Order::find($_COOKIE['order_id']) : new Order();
        if (!is_null($order)) {
            $order_content = json_decode($order->content);

            if (!is_null($order_content)) {
                foreach ($order_content as $prod) {
                    $cart[$prod->id] = $prod->quantity;
                }
            }
        }

        foreach ($products as $product) {
            $product->quantity = isset($cart[$product->id]) ? $cart[$product->id] : 0;
            $product->category_link = $product->categories->first()->getUrl();
            $product->category_name = $product->categories->first()->name;
        }
        return $products->toJson();
    }

    public function getTopProducts()
    {
        $products = Product::where('status', 'public')->where('top', 1)->with(['images' => function ($query) {
            $query->where('type', 'miniature');
        }])->get();

        $cart = [];
        $order = isset($_COOKIE['order_id']) ? Order::find($_COOKIE['order_id']) : new Order();
        if (!is_null($order)) {
            $order_content = json_decode($order->content);

            if (!is_null($order_content)) {
                foreach ($order_content as $prod) {
                    $cart[$prod->id] = $prod->quantity;
                }
            }
        }

        foreach ($products as $product) {
            $product->quantity = isset($cart[$product->id]) ? $cart[$product->id] : 0;
            $product->category_link = $product->categories->first()->getUrl();
            $product->category_name = $product->categories->first()->name;
        }
        return $products->toJson();
    }

}
