<?php

namespace App\Http\Controllers;


use App\Models\Image;
use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image as InterventionImage;


class ImageController extends Controller
{
    public static function uploadImage($type = 'image', $parent_type = 'post', $parent_id = 0, $file = null)
    {
        if ($file != null) {
            $settings = Settings::first();

            $image = new Image;

            $intervention_image = InterventionImage::make($file);

            $filename = str_random(10);

            $thumbnail_size = explode('x', $settings->thumbnail_size);
            $small_size = explode('x', $settings->small_size);
            $medium_size = explode('x', $settings->medium_size);
            $large_size = explode('x', $settings->large_size);

            if ($intervention_image->width() > $intervention_image->height()) {
                $thumbnail_size[1] = $small_size[1] = $medium_size[1] = $large_size[1] = null;
            } elseif ($intervention_image->width() < $intervention_image->height()) {
                $thumbnail_size[0] = $small_size[0] = $medium_size[0] = $large_size[0] = null;
            }

            $full_path = 'images/full/' . $filename . '.jpg';
            $thumbnail_path = 'images/thumbnail/' . $filename . '.jpg';
            $small_path = 'images/small/' . $filename . '.jpg';
            $medium_path = 'images/medium/' . $filename . '.jpg';
            $large_path = 'images/large/' . $filename . '.jpg';

            $intervention_image->interlace()->save($full_path);

            if($settings->fit_thumbnail == '1') {
                InterventionImage::make($file)->fit($thumbnail_size[0], $thumbnail_size[1])->interlace()->save($thumbnail_path);
            } else {
                InterventionImage::make($file)->resize($thumbnail_size[0], $thumbnail_size[1], function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->interlace()->save($thumbnail_path);
            }

            InterventionImage::make($file)->resize($small_size[0], $small_size[1], function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->interlace()->save($small_path);

            InterventionImage::make($file)->resize($medium_size[0], $medium_size[1], function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->interlace()->save($medium_path);

            InterventionImage::make($file)->resize($large_size[0], $large_size[1], function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->interlace()->save($large_path);

            $image->type = $type;
            $image->full = "/" . $full_path;
            $image->thumbnail = "/" . $thumbnail_path;
            $image->small = "/" . $small_path;
            $image->medium = "/" . $medium_path;
            $image->large = "/" . $large_path;

            $image->parent_type = $parent_type;
            $image->parent_id = $parent_id;
            $image->user_id = Auth::user()->id;
            $image->save();

            return $image->id;
        }

    }

    public static function massiveUpload(Request $request, $field, $type, $parent_type, $id)
    {
        foreach ($request[$field] as $image) {
            self::uploadImage($type, $parent_type, $id, $image);
        }
    }
    public static function massiveDelete($ids)
    {
        foreach ($ids as $id) {
            self::deleteImage($id);
        }
    }

    public function ajaxUpload(Request $request)
    {
        $id = self::uploadImage($request->type, $request->parent_type, $request->id, $request->image);

        return Image::find($id)->large;
    }

    public static function deleteImage($id)
    {

        $image = Image::find($id);

        if(file_exists(public_path($image->thumbnail)))
        unlink(public_path($image->thumbnail));

        if(file_exists(public_path($image->small)))
        unlink(public_path($image->small));

        if(file_exists(public_path($image->medium)))
        unlink(public_path($image->medium));

        if(file_exists(public_path($image->large)))
        unlink(public_path($image->large));

        if(file_exists(public_path($image->full)))
        unlink(public_path($image->full));

        $image->delete();
    }


}
