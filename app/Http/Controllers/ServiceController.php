<?php

namespace App\Http\Controllers;

use App\Models\Shop\Product;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ServiceController extends Controller
{
    public function generateSlugForProducts()
    {
        foreach (Product::get() as $product) {
            $product->slug = str_slug($product->name);
            $product->save();
        }
        dump("DONE");
    }

    public function generateSlug(Request $request)
    {
        return str_slug($request->slug_string);
    }

    public function setTag(Request $request)
    {
        $object = $request->type::find($request->id);
        if (isset($request->selected)) {
            $object->categories()->attach($request->selected);
        } elseif (isset($request->deselected)) {
            $object->categories()->detach($request->deselected);
        } else {
            return Response("Request doesn't contain tag info");
        }
        return Response("success");
    }

    public function deleteAllProducts()
    {
        $products = Product::withTrashed()->get();
        foreach ($products as $product) {
            $product->categories()->detach();
            if ($product->trashed()) {
                foreach ($product->images as $image) {
                    $image->deleteWithFiles();
                }
                $product->forceDelete();
            } else {
                $product->delete();
            }
        }
    }
}
