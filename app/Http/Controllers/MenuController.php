<?php

namespace App\Http\Controllers;

use App\Models\MenuZone;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index(Request $request)
    {
        if(MenuZone::all()->isEmpty() || (isset($request->edit) && $request->edit == 0)) {
            $zone = new MenuZone();
            $zone->name = 'Новое меню';
            $zone->save();
        } elseif(isset($request->edit) && $request->edit != 0) {
            $zone = MenuZone::findOrFail($request->edit);
        } else {
            $zone = MenuZone::first();
        }

        return view('admin.pages.menus.form', [
            'zones' => MenuZone::all(),
            'title' => ' | Управление меню',
            'cur_zone' => $zone
        ]);
    }

    public function update(Request $request, $id)
    {
        $zone = MenuZone::find($id);
        $zone->name = $request->name;
        $zone->save();
        return redirect(route('menus.index', ['edit' => $id]));
    }
}
