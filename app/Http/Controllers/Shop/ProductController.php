<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\ImageController;
use App\Models\Shop\Order;
use App\Models\Shop\Product;
use App\Models\Shop\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('trashed')) {
            $products = Product::onlyTrashed();
        } else {
            $products = Product::whereNotNull('user_id');
        }

        if (isset($request->status)) {
            $products->where('status', $request->status);
        }

        if (isset($request->category)) {

            $products = Product::WhereHas('categories', function ($query) use ($request) {
                $query->where('slug', 'LIKE', $request->category);
            });
        }

        if (isset($request->user_id)) {
            $products->where('user_id', $request->user_id);
        }

        $categories = ProductCategory::withTrashed()->where('type', 'category')->get();
        return view('admin.shop.pages.products.list', [

            'products' => $products->paginate(50),
            'categories' => $categories,
            'title' => ' | Список товаров',
            'tags' => ProductCategory::where('type', 'tag')->where('parent_id', null)->where('name', '<>', null)->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request['user_id'] = Auth::user()->id;
        $product = new Product($request->all());
        $product->id = 0;

        return view('admin.shop.pages.products.form', [
            'product' => $product,
            'title' => '| Создание товара',
            'product_categories' => ProductCategory::where('type', 'category')->where('parent_id', null)->where('name', '<>', null)->get(),
            'product_tags' => ProductCategory::where('type', 'tag')->where('parent_id', null)->where('name', '<>', null)->get(),
            'active' => 'Новый товар'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->user_id = Auth::user()->id;
        $product->save();
        return Response($product->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::withTrashed()->where('id', $id)->first();

        return view('admin.shop.pages.products.form', [
            'product' => $product,
            'title' => '| Редактирование товара | ' . $product->name,
            'product_categories' => ProductCategory::where('type', 'category')->where('parent_id', null)->where('name', '<>', null)->get(),
            'product_tags' => ProductCategory::where('type', 'tag')->where('parent_id', null)->where('name', '<>', null)->get(),
            'active' => $product->name ? $product->name : 'Новый товар'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::withTrashed()->where('id', $id)->first();
        $product->fill($request->all());

        $product->categories()->sync($request->categories);

        if (isset($request->image)) {
            if ($product->miniature()) {
                ImageController::deleteImage($product->miniature()->id);
            }
            ImageController::uploadImage('miniature', 'product', $id, $request->image);
        }

        if (isset($request->gallery)) {
            ImageController::massiveUpload($request, 'gallery', 'gallery', 'product', $id);
        }

        if (isset($request->img_delete)) {
            ImageController::massiveDelete($request->img_delete);
        }

        $product->save();

        return redirect(route('products.edit', $product->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return String
     */
    public function destroy($id)
    {
        $product = Product::withTrashed()->where('id', $id)->first();
        if ($product->trashed()) {
            foreach ($product->images as $image) {
                $image->deleteWithFiles();
            }
            $product->forceDelete();
        } else {
            $product->delete();
        }
        return "ok";
    }

    /**
     * Restore deleted post.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $product = Product::withTrashed()->where('id', $id)->first();
        $product->restore();
        return Redirect::back();
    }


    //public function getProductsJSON(Request $request)
    public function getProductsJSON()
    {
        /*$cart_products = [];
        $order = Order::find($request->params['order_id']);
        $cart = json_decode($order->content, true);
        if ($cart != null) {
            foreach ($cart as $product) {
                $cart_products[] = $product->number;
            }

        }*/

        $products = Product::with(['images' => function ($query) {
            $query->where('type', 'miniature');
        }])->take(5);
        /*
                if($cart != null) {
                    $products->whereIn('id', $cart_products, 'or');
                }*/

        $products->get();

        return $products;
    }

    public function changeStatus(Request $request)
    {
        $entity = Product::find($request->id);
        if ($entity->status == 'public') {
            $entity->status = 'hidden';
        } else {
            $entity->status = 'public';
        }
        if ($entity->save()) {
            return "success";
        }
    }

    public function changeSold(Request $request)
    {
        $entity = Product::find($request->id);
        if ($entity->sold == 1) {
            $entity->sold = 0;
        } else {
            $entity->sold = 1;
        }
        if ($entity->save()) {
            return "success";
        }
    }

    public function changeTop(Request $request)
    {
        $entity = Product::find($request->id);
        if ($entity->top == 1) {
            $entity->top = 0;
        } else {
            $entity->top = 1;
        }
        if ($entity->save()) {
            return "success";
        }
    }
}
