<?php

namespace App\Http\Controllers\Shop;


use App\Models\Shop\ProductCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\ImageController;


class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('trashed')) {
            $product_category = ProductCategory::onlyTrashed();
        } else {
            $product_category = ProductCategory::whereNotNull('user_id');
        }
        if(isset($request->type)) {
            $product_category->where('type', $request->type);
        }
        if(isset($request->status)) {
            $product_category->where('status', $request->status);
        }

        if(isset($request->user_id)) {
            $product_category->where('user_id', $request->user_id);
        }

        $product_category->where('parent_id', null);
        return view('admin.shop.pages.product_categories.list',[
            'product_categories' => $product_category->get(),
            'title' => ' | Список категорий',
            'active' => 'Категории'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request['user_id'] = Auth::user()->id;
        $product_category = new ProductCategory($request->all());
        $product_category->id = 0;
        $product_category->type = $request->type;
        return view('admin.shop.pages.product_categories.form',[
            'product_category' => $product_category,
            'title' => '| Создание категории',
            'product_categories' => ProductCategory::whereNotIn('id', [$product_category->id])->where('name', '<>', null)->get(),
            'active' => 'Новая категория'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product_category = new ProductCategory();
        $product_category->name = $request->name;
        $product_category->type = $request->type;
        $product_category->user_id = Auth::user()->id;
        $product_category->save();
        return Response($product_category->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_category = ProductCategory::withTrashed()->where('id', $id)->first();

        return view('admin.shop.pages.product_categories.form',[
            'product_category' => $product_category,
            'title' => '| Редактирование категории | '. $product_category->name,
            'product_categories' => ProductCategory::whereNotIn('id', [$id])->where('name', '<>', null)->get(),
            'active' => $product_category->name ? $product_category->name : 'Новая категория'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product_category = ProductCategory::withTrashed()->where('id', $id)->first();
        $product_category->fill($request->all());
        if(isset($request->image)){
            if($product_category->miniature()) {
                ImageController::deleteImage($product_category->miniature()->id);
            }
            ImageController::uploadImage( 'miniature', 'product_category', $id, $request->image);
        }

        if(isset($request->gallery)){
            ImageController::massiveUpload($request, 'gallery', 'gallery','product_category', $id);
        }

        if(isset($request->img_delete)){
            ImageController::massiveDelete($request->img_delete);
        }
        $product_category->save();

        return redirect(route('product_categories.edit', $product_category->id));
    }

    public function destroy($id)
    {
        $product_category = ProductCategory::withTrashed()->where('id', $id)->first();
        if ($product_category->trashed()) {
            $product_category->forceDelete();
            return redirect(route('product_categories.index'));
        } else {
            $product_category->delete();
            return Redirect::back();
        }
    }

    /**
     * Restore deleted ProductCategory::.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $product_category = ProductCategory::withTrashed()->where('id', $id)->first();
        $product_category->restore();
        return Redirect::back();
    }
}
