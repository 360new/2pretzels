@extends('admin.index')
@section('styles')
    <!-- Summernote css -->
    <link rel="stylesheet" href="/admin-files/vendors/bower_components/summernote/dist/summernote.css"/>
@endsection
@section('content')

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Редактирование записи</h5>
            </div>

            @component('admin.components.breadcrumbs')
                @slot('parent_route') {{route('posts.index',['type' => $post->type])}} @endslot
                @slot('parent') Записи @endslot
                @slot('active') {{$active}} @endslot
            @endcomponent

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <a role="button" data-toggle="collapse" href="#collapsePost" aria-expanded="true"
                                   aria-controls="collapsePost" class="d-block">
                                </a>
                            </div>
                            <div class="panel-wrapper collapse in" id="collapsePost">
                                <div class="panel-body">
                                    <div class="form-wrap mt-10">
                                        <form name="form-data" id="form-data" enctype="multipart/form-data"
                                              action="{{route('posts.update', $post->id)}}"
                                              method="POST">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Название</label>
                                                <input type="text" class="form-control" value="{{$post->title}}"
                                                       name="title"
                                                       placeholder="Новая запись">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Ссылка</label>
                                                <input type="text" class="form-control" value="{{$post->slug}}"
                                                       name="slug"
                                                       placeholder="new_post">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Содержимое</label>
                                                <textarea class="summernote"
                                                          name="content">{{$post->content}}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Дополнительное
                                                    содержмое</label>
                                                <textarea class="form-control" rows="10"
                                                          name="additional">{{$post->additional}}</textarea>
                                            </div>
                                            <input type="hidden" name="id" value="{{$post->id}}">
                                            <input type="hidden" name="user_id" value="{{$post->user_id}}">
                                            <input type="hidden" name="type" value="{{$post->type}}">
                                            @method('PUT')
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @component('admin.components.panels.gallery_panel',
                   ['gallery' => $post->gallery(), 'id' => $post->id])
                    @endcomponent

                    @component('admin.components.panels.seo_panel')
                        @slot('keywords') {{$post->keywords}} @endslot
                        @slot('description') {{$post->descripion}} @endslot
                    @endcomponent
                </div>
            </div>

            <div class="col-md-3">
                <div class="row">
                    @component('admin.components.blocks.control_block',
                    ['deleted_at' => $post->deleted_at])
                        @slot('created_at') {{$post->created_at}} @endslot
                        @slot('status') {{$post->status}} @endslot
                    @endcomponent

                    @component('admin.components.blocks.miniature_block',
                    ['miniature' => $post->miniature(), 'id' => $post->id])
                    @endcomponent

                    @component('admin.components.blocks.categories_block',
                     ['object' => $post, 'object_categories' => $categories])
                    @endcomponent
                    @component('admin.components.blocks.tags_block',
                       ['object' => $post, 'object_tags' => $tags])
                    @endcomponent
                </div>
            </div>

            <!-- /Row -->


        </div>
        @endsection

        @component('admin.components.modal_delete')
            @slot('modal_text') {{is_null($post->deleted_at)? 'убрать публикацию в корзину': 'окончательно удалить публикацию'}}  @endslot
            @slot('modal_caption') {{is_null($post->deleted_at)? 'Убрать': 'Удалить'}} @endslot
        @endcomponent
        @if(!is_null($post->deleted_at))
            @component('admin.components.modal_restore')
                @slot('modal_target') запись @endslot
            @endcomponent
        @endif
        @section('scripts')
            <script>$(".chosen-select").chosen({
                    width: "100%",
                });</script>
            <!-- Summernote Plugin JavaScript -->
            <script src="/admin-files/vendors/bower_components/summernote/dist/summernote.min.js"></script>
            <script src="/admin-files/vendors/bower_components/summernote/lang/summernote-ru-RU.js"></script>
            <script>
                $('.summernote').summernote({
                    lang: 'ru-RU',
                    height: 300,
                    callbacks: {
                        onImageUpload: function (files) {
                            var el = $(this);
                            sendFile(files[0], el);
                        }
                    }
                });

                function sendFile(file, el) {
                    var data = new FormData();
                    data.append("image", file);
                    data.append("type", 'content_image');
                    data.append("parent_type", 'post');
                    data.append("parent_id", $('input[name="id"]').val());
                    var url = '{{ route('image.upload') }}';
                    $.ajax({
                        data: data,
                        type: "POST",
                        headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                        url: url,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (link) {
                            el.summernote('insertImage', link);
                        }
                    });
                }


                <!-- End Summernote-->

                $(".btn-del").click(function () {
                    window.location.href = "/admin/posts/" + $('input[name="id"]').val() + "/delete";
                });
                $(".btn-rest").click(function () {
                    window.location.href = "/admin/posts/" + $('input[name="id"]').val() + "/restore";
                });

                $('input[name="title"]').blur(function () {
                    if ($('input[name="slug"]').val().length === 0) {
                        $.get("{{route('generate.slug')}}", {
                            _token: "{{csrf_token()}}",
                            slug_string: $('input[name="title"]').val()
                        }, function (data) {
                            $('input[name="slug"]').val(data);
                        });
                    }

                    if ($('input[name="id"]').val() === "0") {

                        $.post("{{route('posts.store')}}", {
                            _token: "{{csrf_token()}}",
                            title: $('input[name="title"]').val(),
                            type: $('input[name="type"]').val()
                        }, function (data) {
                            $('input[name="id"]').val(data);
                            $('input[name="gallery\\[\\]"]').prop("disabled", false);
                            $('input[name="image"]').prop("disabled", false);
                            $('#form-data').attr("action", "/admin/posts/" + data);

                        });
                    }
                });


            </script>

@endsection