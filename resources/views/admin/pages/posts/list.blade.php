@extends('admin.index')
@section('styles')
@endsection
@section('content')

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Cписок публикаций</h5>
                @if(request()->has('trashed'))
                    <a href="{{route('posts.index', ['type'=> request()->input('type')])}}" class="ml-5">К
                        опубликованному</a>
                @else
                    <a href="{{route('posts.index', ['type'=> request()->input('type')])}}&trashed" class="ml-5">В
                        корзину</a>
                @endif
            </div>
            @component('admin.components.breadcrumbs')
                @slot('active') {{$active}} @endslot
            @endcomponent
        </div>
        <!-- /Title -->

        <div class="row">

            <!-- Bordered Table -->
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>Название</th>
                                            <th>Дата</th>
                                            <th>Автор</th>
                                            <th class="text-nowrap">Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($posts as $post )
                                            <tr data-id="{{$post->id}}">
                                                <td>
                                                    <a href="{{route('posts.edit', $post->id)}}">{{is_null($post->title)?'Новая запись': $post->title}}</a>
                                                </td>
                                                <td>{{$post->created_at}}</td>
                                                <td>{{$post->user->name}}</td>
                                                <td class="text-nowrap">
                                                    <a href="{{route('posts.edit', $post->id)}}" class="mr-10"
                                                       data-toggle="tooltip" data-original-title="Редактировать"> <i
                                                                class="fa fa-pencil text-inverse"></i> </a>
                                                    <a href="#" data-toggle="tooltip" class="mr-10"
                                                       data-original-title="Убрать"> <i
                                                                class="fa fa-close btn-modal-close text-danger"></i>
                                                    </a>
                                                    @if(!is_null($post->deleted_at))
                                                        <a href="#" data-toggle="tooltip" class="mr-10"
                                                           data-original-title="Восстановить"> <i
                                                                    class="fa fa-arrow-up btn-modal-restore text-success"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Bordered Table -->

        </div>

    </div>
@endsection

@component('admin.components.modal_delete')
    @slot('modal_text') {{!request()->has('trashed') ? 'убрать публикацию в корзину': 'окончательно удалить публикацию'}}  @endslot
    @slot('modal_caption') {{!request()->has('trashed') ? 'Убрать': 'Удалить'}} @endslot
@endcomponent

@if(request()->has('trashed'))
    @component('admin.components.modal_restore')
        @slot('modal_target') запись @endslot
    @endcomponent
@endif
@section('scripts')
    <script>
        $(".btn-modal-close").click(function () {
            var id = $(this).closest('tr').data('id');
            console.log(id);
            $("#delModal").modal();
            $(".btn-del").click(function () {
                window.location.href = "/admin/posts/" + id + "/delete  ";
            });
        });

        $(".btn-modal-restore").click(function () {
            var id = $(this).closest('tr').data('id');
            console.log(id);
            $("#restoreModal").modal();
            $(".btn-rest").click(function () {
                window.location.href = "/admin/posts/" + id + "/restore";
            });
        });
    </script>


@endsection