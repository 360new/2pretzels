@extends('admin.index')
@section('styles')
    <!-- Summernote css -->
    <link rel="stylesheet" href="/admin-files/vendors/bower_components/summernote/dist/summernote.css"/>
@endsection
@section('content')

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Редактирование поста</h5>
            </div>

            @component('admin.components.breadcrumbs')
                @slot('active') {{$active}} @endslot
            @endcomponent

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <a role="button" data-toggle="collapse" href="#collapsePost" aria-expanded="true"
                                   aria-controls="collapsePost" class="d-block">
                                    <h6 class="panel-title txt-dark">Настройки</h6>
                                </a>
                            </div>
                            <div class="panel-wrapper collapse in" id="collapsePost">
                                <div class="panel-body">
                                    <div class="form-wrap mt-12">
                                        <form name="form-data" id="form-data" enctype="multipart/form-data"
                                              action="{{route('settings.update')}}"
                                              method="POST">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Название сайта</label>
                                                <input type="text" class="form-control" value="{{$settings->site_name}}"
                                                       name="site_name"
                                                       placeholder="Ещё один чудесный сайт">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Контактный email</label>
                                                <input type="email" class="form-control" value="{{$settings->contacts_email}}"
                                                       name="contacts_email"
                                                       placeholder="user@mail.ru">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Поле для вставки кода на сайт</label>
                                                <textarea class="form-control " name="code" cols="30" rows="10">{{$settings->code}}</textarea>
                                            </div>
                                            <hr>
                                            <h4 class="mb-10">SEO параметры</h4>
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Ключевые слова по умолчанию</label>
                                                <input type="text" class="form-control" value="{{$settings->default_keywords}}"
                                                       name="default_keywords"
                                                       placeholder="Сайт, тема, информация">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Мета-описание по умолчанию</label>
                                                <textarea class="form-control " name="default_description" id="" cols="30" rows="15">{{$settings->default_description}}</textarea>
                                            </div>
                                            <hr>
                                            <h4 class="mb-10">Параметры изображений</h4>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <h6>Миниатюры</h6>
                                                        <div class="row mb-10">
                                                            <div class="col-xs-6"><input type="number" value="{{$sizes['thumbnail'][0]}}" name="sizes[thumbnail][0]" class="mw-100"> x <input type="number" class="mw-100" value="{{$sizes['thumbnail'][1]}}" name="sizes[thumbnail][1]"></div>
                                                            <div class="col-xs-6"><label>
                                                                    <input type="hidden" name="fit_thumbnail" value="0">
                                                                    <input type="checkbox" name="fit_thumbnail" @if($settings->fit_thumbnail == 1) checked @endif value="1" > Фиксированный размер </label></div>
                                                        </div>
                                                        <h6>Малый размер</h6>
                                                        <div class="row mb-10">
                                                            <div class="col-xs-6"><input type="number" class="mw-100" value="{{$sizes['small'][0]}}" name="sizes[small][0]"> x <input type="number" class="mw-100" value="{{$sizes['small'][1]}}" name="sizes[small][1]"> </div>
                                                        </div>
                                                        <h6>Средний размер</h6>
                                                        <div class="row mb-10">
                                                            <div class="col-xs-6"><input type="number" class="mw-100" value="{{$sizes['medium'][0]}}" name="sizes[medium][0]"> x <input type="number" class="mw-100" value="{{$sizes['medium'][1]}}" name="sizes[medium][1]"></div>
                                                        </div>
                                                        <h6>Большой размер</h6>
                                                        <div class="row mb-10">
                                                            <div class="col-xs-6"><input type="number" class="mw-100" value="{{$sizes['large'][0]}}" name="sizes[large][0]"> x <input type="number" class="mw-100" value="{{$sizes['large'][1]}}" name="sizes[large][1]"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="panel-text">
                                                            <p>&nbsp;&nbsp;&nbsp;&nbsp;Система хранит 5 версий изображений. Оригинал загружается "как есть", а параметры создания остальных версий вы можете указать здесь.
                                                                При сохранении картинки система будет применять указанные размеры для большего параметра.
                                                                Таким образом, если ширина изображения больше высоты, ограничения применятся для шириниы, а высота будет подобрана автоматически.</p>
                                                            <p>&nbsp;&nbsp;&nbsp;&nbsp;Для миниатюр вы можете зафиксировать размер, в таком случае размеры изображения будут точно соответствовать параметрам</p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Сохранить настройки сайта</button>
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /Row -->

        </div>
        @endsection