@extends('admin.index')
@section('styles')
@endsection
@section('content')

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Cписок категорий товаров</h5>
                @if(request()->has('trashed'))
                    <a href="{{route('product_categories.index')}}" class="ml-5">К
                        опубликованному</a>
                @else
                    <a href="{{route('product_categories.index')}}?trashed" class="ml-5">В
                        корзину</a>
                @endif
            </div>
            @component('admin.components.breadcrumbs')
                @slot('active') {{$active}} @endslot
            @endcomponent
        </div>
        <!-- /Title -->

        <div class="row">

            <!-- Bordered Table -->
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-heading">
                            <div class="pull-left">
                                @component('admin.components.paging.filter')@endcomponent
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>Название</th>
                                            <th>Дата</th>
                                            <th>Автор</th>
                                            <th class="text-nowrap">Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php buildCategoriesList($product_categories, 'product_categories')?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Bordered Table -->

        </div>

    </div>
@endsection

@component('admin.components.modal_delete')
    @slot('modal_text') {{!request()->has('trashed') ? 'убрать категорию в корзину': 'окончательно удалить категорию'}}  @endslot
    @slot('modal_caption') {{!request()->has('trashed') ? 'Убрать': 'Удалить'}} @endslot
@endcomponent

@if(request()->has('trashed'))
    @component('admin.components.modal_restore')
        @slot('modal_target') категорию @endslot
    @endcomponent
@endif
@section('scripts')
    <script>
        $(".btn-modal-close").click(function () {
            var id = $(this).closest('tr').data('id');
            console.log(id);
            $("#delModal").modal();
            $(".btn-del").click(function () {
                window.location.href = "/admin/product_categories/" + id + "/delete  ";
            });
        });

        $(".btn-modal-restore").click(function () {
            var id = $(this).closest('tr').data('id');
            console.log(id);
            $("#restoreModal").modal();
            $(".btn-rest").click(function () {
                window.location.href = "/admin/product_categories/" + id + "/restore";
            });
        });
    </script>


@endsection