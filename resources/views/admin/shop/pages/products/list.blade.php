@extends('admin.index')
@section('styles')
@endsection
@section('content')

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Cписок товаров</h5>
                @if(request()->has('trashed'))
                    <a href="{{route('products.index')}}" class="ml-5">К
                        опубликованному</a>
                @else
                    <a href="{{route('products.index')}}?trashed" class="ml-5">В
                        корзину</a>
                @endif
            </div>
            @component('admin.components.breadcrumbs')
                @slot('active')Товары@endslot
            @endcomponent
        </div>
        <!-- /Title -->

        <div class="row">

            <!-- Bordered Table -->
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <!--TODO: вынести фильтр в отдельный компонент. А лучше - использовать один компонент для всех сущностей -->
                                <form action="" method="get">
                                    <div class="row">
                                        <div class="col-xs-6 pl-0">
                                            <select name="category" class="form-control">
                                                <option value="">Все</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->slug}}"
                                                            @if(request()->category == $category->slug) selected @endif>{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-xs-2">
                                            <button class="btn btn-primary" type="submit">Показать</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th style="width:64px;">
                                                <div class="text-center"><i class="zmdi zmdi-image"></i></div>
                                            </th>
                                            <th>Название</th>
                                            <th>Категории</th>
                                            <th>Теги</th>
                                            <th class="text-nowrap">Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $product )
                                            <tr data-id="{{$product->id}}">
                                                <td style="width:64px;">
                                                    <a href="{{route('products.edit', $product->id)}}">
                                                        @if(is_object($product->miniature()))
                                                            <img src="{{$product->miniature()->thumbnail}}" alt=""
                                                                 height="32">
                                                        @else
                                                            <img src="http://via.placeholder.com/32x32" alt="">
                                                        @endif
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{route('products.edit', $product->id)}}">{{is_null($product->name)?'Новый товар': $product->name}}</a>
                                                </td>

                                                <td>@foreach($product->categories->where('type', 'category') as $cat)
                                                        <a href="{{route('product_categories.edit', $cat->id)}}">{{$cat->name}}</a>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    <select data-placeholder="Выберите тег" class="chosen-select"
                                                            data-product="{{$product->id}}" multiple>
                                                        @foreach($tags as $tag)
                                                            <option value="{{$tag->id}}"
                                                                    @if($product->categories->contains($tag->id))
                                                                    selected
                                                                    @endif>{{$tag->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td class="text-nowrap">

                                                    <a href="#!" data-toggle="tooltip" class="mr-10 ="
                                                       data-original-title="Популярность товара">
                                                        <i class="fa {{ $product->top == 1 ?"fa-star":"fa-star-half"}} site-top-button"></i>
                                                    </a>

                                                    <a href="#!" data-toggle="tooltip" class="mr-10 ="
                                                       data-original-title="Видимость на сайте">
                                                        <i class="fa {{ $product->status == "public"?"fa-eye":"fa-eye-slash"}} site-visible-button"></i>
                                                    </a>

                                                    <a href="#!" data-toggle="tooltip" class="mr-10 ="
                                                       data-original-title="Доступность для покупки">
                                                        <i class="fa {{ $product->sold == 1 ?"fa-lock":"fa-unlock"}} site-available-button"></i>
                                                    </a>

                                                    <a href="{{route('products.edit', $product->id)}}" class="mr-10"
                                                       data-toggle="tooltip" data-original-title="Редактировать"> <i
                                                                class="fa fa-pencil text-inverse"></i> </a>
                                                    <a href="#" data-toggle="tooltip" class="mr-10"
                                                       data-original-title="Удалить"><i
                                                                class="fa fa-close btn-modal-close text-danger"></i></a>
                                                    @if(!is_null($product->deleted_at))
                                                        <a href="#" data-toggle="tooltip" class="mr-10"
                                                           data-original-title="Восстановить"> <i
                                                                    class="fa fa-arrow-up btn-modal-restore text-success"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="text-center">
                                {!! $products->render()!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Bordered Table -->

        </div>

    </div>
@endsection

@component('admin.components.modal_delete')
    @slot('modal_text') {{!request()->has('trashed') ? 'убрать товар в корзину': 'окончательно удалить товар'}}  @endslot
    @slot('modal_caption') {{!request()->has('trashed') ? 'Убрать': 'Удалить'}} @endslot
@endcomponent

@if(request()->has('trashed'))
    @component('admin.components.modal_restore')
        @slot('modal_target') товар @endslot
    @endcomponent
@endif
@section('scripts')
    <script>
        $(".chosen-select").chosen({
            width: "100%",
        }).on('change', function (evt, params) {
            console.log();
            params.id = $(evt.target).data('product');
            params.type = "App\\Models\\Shop\\Product";
            params._token = "{{csrf_token()}}";
            $.post("{{route('set.tag')}}", params, function (data) {

            });
        });

        $(".btn-modal-close").click('change',function () {

            var product_row = $(this).closest('tr');
            var id = product_row.data('id');
            console.log(id);
            $("#delModal").modal();

            $(".btn-del").click(function () {

                $.get("/admin/products/" + id + "/delete  ", function (data) {
                    if(data === "ok") {
                        product_row.fadeOut(500);
                    }
                });
            });
        });

        $(".btn-modal-restore").click(function () {
            var id = $(this).closest('tr').data('id');
            $("#restoreModal").modal();
            $(".btn-rest").click(function () {
                window.location.href = "/admin/products/" + id + "/restore";
            });
        });

        $(".site-visible-button").click(function () {
            event.preventDefault();
            var button = $(this);
            var id = button.closest('tr').data('id');
            $.post("{{route('products.status-change')}}", {
                id: id,
                _token: "{{csrf_token()}}"
            }, function (data) {
                if(data === "success") {
                    if(button.hasClass('fa-eye')) {
                        button.removeClass('fa-eye').addClass('fa-eye-slash');
                    } else {
                        button.removeClass('fa-eye-slash').addClass('fa-eye');
                    }
                }
            });
        });

        $(".site-available-button").click(function () {
            event.preventDefault();
            var button = $(this);
            var id = button.closest('tr').data('id');
            $.post("{{route('products.sold-change')}}", {
                id: id,
                _token: "{{csrf_token()}}"
            }, function (data) {
                if(data === "success") {
                    if(button.hasClass('fa-lock')) {
                        button.removeClass('fa-lock').addClass('fa-unlock');
                    } else {
                        button.removeClass('fa-unlock').addClass('fa-lock');
                    }
                }
            });
        });

        $(".site-top-button").click(function () {
            event.preventDefault();
            var button = $(this);
            var id = button.closest('tr').data('id');
            $.post("{{route('products.top-change')}}", {
                id: id,
                _token: "{{csrf_token()}}"
            }, function (data) {
                if(data === "success") {
                    if(button.hasClass('fa-star-half')) {
                        button.removeClass('fa-star-half').addClass('fa-star');
                    } else {
                        button.removeClass('fa-star').addClass('fa-star-half');
                    }
                }
            });
        });
    </script>

@endsection