<form action="" method="get">
    <div class="row">
        <div class="col-xs-6 pl-0">
            <select name="type" class="form-control">
                <option value="category" @if(request()->type == "category") selected @endif>Категории</option>
                <option value="tag" @if(request()->type == "tag") selected @endif>Теги</option>
            </select>
        </div>

        <div class="col-xs-2">
            <button class="btn btn-primary" type="submit">Показать</button>
        </div>
    </div>
</form>