<div class="col-md-12">
    <div class="panel panel-default card-view">
        <div class="panel-heading">
            <a role="button" data-toggle="collapse" href="#collapseCategories" aria-expanded="true"
               aria-controls="collapseCategories" class="d-block">
                <h6 class="panel-title txt-dark">Категории</h6>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="panel-wrapper collapse in" id="collapseCategories">
            <div class="panel-body">
                <div class="form-wrap categories-holder">
                    {{buildCategoriesTree($object_categories, $object)}}
                </div>
            </div>
        </div>
    </div>
</div>