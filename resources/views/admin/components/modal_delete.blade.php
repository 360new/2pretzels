<div id="delModal" class="modal modal-danger fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="delModalLabel">Подтвердите удаление</h5>
            </div>
            <div class="modal-body">
                <p>Вы действительно хотите {{$modal_text}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-del pull-left"
                        data-dismiss="modal">{{$modal_caption}}</button>
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Отмена</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>