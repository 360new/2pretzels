<div class="col-md-12">
    <div class="panel panel-default card-view">

        <div class="panel-heading pb-20">
            <a role="button" data-toggle="collapse" href="#collapseSeo" aria-expanded="true"
               aria-controls="collapseSeo" class="d-block">
                <h6 class="panel-title txt-dark">SEO параметры</h6>
            </a>
        </div>
        <div class="panel-wrapper collapse in" id="collapseSeo">
            <div class="panel-body">
                <div class="form-wrap">
                    <form>
                        <div class="form-group">
                            <label class="control-label mb-10 text-left">Ключевые слова</label>
                            <input type="text" class="form-control" value="{{$keywords}}"
                                   name="keywords" form="form-data"
                                   placeholder="Главная, тема, статьи">
                        </div>

                        <div class="form-group">
                            <label class="control-label mb-10 text-left">Мета-описание</label>
                            <textarea class="form-control" name="description" id="" cols="30"
                                      rows="10"
                                      form="form-data">{{$description}}</textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>