<div class="col-md-12">
    <div class="panel panel-default card-view">
        <div class="panel-heading pb-20">
            <a role="button" data-toggle="collapse" href="#collapseGallery" aria-expanded="true"
               aria-controls="collapseGallery" class="d-block">
                <h6 class="panel-title txt-dark">Галерея</h6>
            </a>
        </div>
        <div class="panel-wrapper collapse in" id="collapseGallery">
            <div class="panel-body">
                <div class="form-wrap mt-5">
                    <input type="file" name="gallery[]" multiple="true" class="form-control"
                           form="form-data" @if($id == 0) disabled @endif>

                    <p class="mt-10">Отметьте галочкой изображения, которые хотите удалить</p>
                    <div class="row ">
                        @foreach($gallery as $gal)
                            <div class="col-xs-2 gal-item">
                                <input class="img-del" type="checkbox" name="img_delete[]"
                                       form="form-data" value="{{$gal->id}}">
                                <img src="{{$gal->thumbnail}}" alt="">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>