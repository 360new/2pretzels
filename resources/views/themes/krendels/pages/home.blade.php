@extends('themes.krendels.index')
@section('content')
    <div class="page-content home-page-3">
        <section class="our-menu our-menu-style-1 our-menu-style-1-invert">
            <div class="our-menu-wrapper">
                <div class="container">
                    <div class="our-menu-inner">
                        <div class="heading-section text-center">
                            <div class="top-heading p-color-1">
                                <span>Основные</span>
                            </div>
                            <div class="main-heading text-uppercase">
                                <h3 class="text-white">Блюда</h3>
                            </div>
                            <div class="icon-heading ">
                                <img src="/front/themes/krendels/img/icons/ic-plate.png" alt="plate"/>
                            </div>
                        </div>
                        <!-- End Heading Section-->
                    </div>
                </div>
                <div class="our-menu-navbar heading-font text-center m-t-40">
                    <ul class="clear-list-style">
                        <li class="m-l-20 m-r-20 slytablink active ">
                            <span data-toggle="pill" class="slycat cattop">Популярное/Топ</span>
                        </li>
                        @foreach($tabs as $category)
                            <li class="m-l-20 m-r-20 slytablink">
                                <span data-toggle="pill" class="slycat slycat{{$category->id}}">{{$category->name}}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="tab-content"
                     style="background: url('/front/themes/krendels/img/2_bg.jpg') center no-repeat;">

                    <!-- Таб с главными продуктами -->
                    <div class="container our-menu-tab active p-t-40 p-b-40 bg-white p-l-40 p-r-40"
                         id="bltab_pop">
                        <div class="our-menu-tab-inner frame loading" id="mainslyframe"> <!-- Добавляем класс .owl-carousel, и карусель работает -->
                            <ul class="slidee">

                            </ul>
                        </div>
                        <div class="scrollbar" id="scrolltab">
                            <div class="handle"></div>
                        </div>
                        <div class="sly-arrow sly-arrow-left"></div>
                        <div class="sly-arrow sly-arrow-right"></div>
                    </div>

                    </div>
        </section>
        <!-- End Section Menu-->
        <section class="our-store our-store-style-2">
            <div class="our-store-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="our-store-main matchHeight">
                                <div class="our-store-inner">
                                    <div class="heading-section text-center">
                                        <div class="top-heading p-color-1">
                                            <span>Что такое</span>
                                        </div>
                                        <div class="main-heading text-uppercase">
                                            <h3>«Два кренделя»</h3>
                                        </div>
                                        <div class="icon-heading">
                                            <img src="/front/themes/krendels/favicon.png" width="50" alt="plate"/>
                                        </div>
                                    </div>
                                    <!-- End Heading Section-->
                                    <div class="chef-cook text-center m-t-40">
                                        <div class="chef-intro">
                                            <p>Два кренделя — это сеть современных кулинарий-кафе, где
                                                органично сочетаются концепции городского кафе, домашней кухни и
                                                кондитерской.</p>
                                            <!--<p>Когда мы создавали сеть кафе «Два кренделя», мы хотели создать уютное место, с хорошим сервисом, по-настоящему вкусной и недорогой едой, где будет приятно выпить кофе и перекусить в любое время дня. Таких предложений в Москве сейчас не так уж и много, а наши кафе — это как раз такое место — здесь всегда максимальный выбор блюд и отличный сервис, при этом цены не заоблачные.</p>-->
                                            <p>Ежедневно мы готовим и доставляем в наши кафе более 100 видов блюд
                                                — это свежая выпечка, разнообразные десерты, горячие блюда, салаты,
                                                супы, сэндвичи.</p>
                                            <p>Особая гордость — постоянное наличие в меню вегетарианских блюд — от
                                                салатов и закусок до горячего. Все наши блюда мы готовим только из
                                                свежих и натуральных продуктов.</p>
                                        </div>
                                        {{-- <div class="chef-avatar">
                                           <img src="/front/themes/krendels/img/ramsay.jpg" alt="chef cook" />
                                       </div>
                                        <div class="chef-name">
                                           <h4>Виктор Баринов</h4>
                                       </div>
                                       <div class="chef-title text-uppercase">
                                           <span>наш шеф-повар</span>
                                       </div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="our-store-image matchHeight">
                                <img class="" src="/front/themes/krendels/img/flambe.jpg" alt="Какая-то фотка"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section Store-->
        <!-- Кафе -->
        <section class="our-gallery our-gallery-style-1 our-gallery-style-2 our-gallery-style-2-invert"
                 id="lavki_block">
            <div class="our-gallery-wrapper">
                <div class="container clearfix">
                    <div class="heading-section text-center p-t-40">
                        <div class="top-heading p-color-1">
                            <span>Наши</span>
                        </div>
                        <div class="main-heading text-uppercase">
                            <h3 class="text-white">Кафе</h3>
                        </div>
                        <div class="icon-heading">
                            <img src="/front/themes/krendels/favicon.png" width="30" alt="ЛожкиВилки"/>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="our-gallery-inner">
                        <div class="our-menu-navbar heading-font text-center m-t-40">
                            <ul class="clear-list-style">
                                <li class="m-l-20 m-r-20">
                                    <a data-toggle="pill" href="#kaftab1">Список кафе</a>
                                </li>
                                <li class="m-l-20 m-r-20 active">
                                    <a data-toggle="pill" href="#kaftab2">Карта</a>
                                </li>

                            </ul>
                        </div>
                        <div class="tab-content"
                             style="background: url('/front/themes/krendels/img/2_bg.jpg') center no-repeat;">
                            <!-- 1 таб -->
                            <div class="our-menu-tab tab-pane p-t-40 p-b-40 bg-white p-l-70 p-r-70 our-gallery-cafe"
                                 id="kaftab1">
                                <div class="our-gallery-list-wrap">
                                    <div class="our-gallery-list">

                                        <div class="gallery-item gallery-item-style-1">
                                            <div class="gallery-overlay">
                                                <div class="gallery-overlay-phone">
                                                    +7(911)364-36-42<br>
                                                    <a href="{{url("/pages/restaurants/aeroport")}}"
                                                       class="btn btn-outline-light btn-sm">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="gallery-image">
                                                <img class="" src="front/themes/krendels/img/lavki/len_mini.jpg"
                                                     alt="м. Аэропорт, Ленинградский пр., д. 39, стр. 80"/>
                                            </div>
                                            <div class="gallery-content">
                                                <div class="gallery-name heading-font">
                                                    <a class="" href="{{url("/pages/restaurants/aeroport")}}">м.
                                                        Аэропорт</a>
                                                </div>
                                                <div class="gallery-topic">
                                                    Ленинградский пр., д. 39, стр. 80
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gallery-item gallery-item-style-1">
                                            <div class="gallery-overlay">
                                                <div class="gallery-overlay-phone">
                                                    +7(911)364-36-42<br>
                                                    <a href="{{url("/pages/restaurants/kr-vorota")}}"
                                                       class="btn btn-outline-light btn-sm">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="gallery-image">
                                                <img class="" src="front/themes/krendels/img/lavki/kr_vorota.jpg"
                                                     alt="м. Красные ворота, Орликов переулок, д. 5"/>
                                            </div>
                                            <div class="gallery-content">
                                                <div class="gallery-name heading-font">
                                                    <a class="" href="{{url("/pages/restaurants/kr-vorota")}}">м.
                                                        Красные ворота</a>
                                                </div>
                                                <div class="gallery-topic">
                                                    м. Красные ворота, Орликов переулок, д. 5
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gallery-item gallery-item-style-1">
                                            <div class="gallery-overlay">
                                                <div class="gallery-overlay-phone">
                                                    +7(911)364-36-42<br>
                                                    <a href="{{url("/pages/restaurants/technopark")}}"
                                                       class="btn btn-outline-light btn-sm">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="gallery-image">
                                                <img class="" src="front/themes/krendels/img/lavki/texno_mini.jpg"
                                                     alt="м. Технопарк, пр. Андропова, д.18, корп. 5"/>
                                            </div>
                                            <div class="gallery-content">
                                                <div class="gallery-name heading-font">
                                                    <a class="" href="{{url("/pages/restaurants/technopark")}}">м.
                                                        Технопарк</a>
                                                </div>
                                                <div class="gallery-topic">
                                                    м. Технопарк, пр. Андропова, д.18, корп. 5
                                                </div>
                                            </div>
                                        </div>

                                        {{--<div class="gallery-item gallery-item-empty"></div>--}}

                                        <div class="gallery-item gallery-item-style-1">
                                            <div class="gallery-overlay">
                                                <div class="gallery-overlay-phone">
                                                    +7(911)364-36-42<br>
                                                    <a href="{{url("/pages/restaurants/trubnaya")}}"
                                                       class="btn btn-outline-light btn-sm">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="gallery-image">
                                                <img class="" src="front/themes/krendels/img/lavki/tru_mini.jpg"
                                                     alt="м. Трубная, ул. Трубная, д.17, стр. 1"/>
                                            </div>
                                            <div class="gallery-content">
                                                <div class="gallery-name heading-font">
                                                    <a class="" href="{{url("/pages/restaurants/trubnaya")}}">м.
                                                        Трубная</a>
                                                </div>
                                                <div class="gallery-topic">
                                                    м. Трубная, ул. Трубная, д.17, стр. 1
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gallery-item gallery-item-style-1">
                                            <div class="gallery-overlay">
                                                <div class="gallery-overlay-phone">
                                                    +7(911)364-36-42<br>
                                                    <a href="{{url("/pages/restaurants/park-kultury")}}"
                                                       class="btn btn-outline-light btn-sm">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="gallery-image">
                                                <img class="" src="front/themes/krendels/img/lavki/roza_mini.jpg"
                                                     alt="м. Парк культуры, ул. Льва Толстого, д. 16"/>
                                            </div>
                                            <div class="gallery-content">
                                                <div class="gallery-name heading-font">
                                                    <a class="" href="{{url("/pages/park-kultury")}}">м. Парк
                                                        культуры</a>
                                                </div>
                                                <div class="gallery-topic">
                                                    м. Парк культуры, ул. Льва Толстого, д. 16
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gallery-item gallery-item-style-1">
                                            <div class="gallery-overlay">
                                                <div class="gallery-overlay-phone">
                                                    +7(911)364-36-42<br>
                                                    <a href="{{url("/pages/taganskaya")}}"
                                                       class="btn btn-outline-light btn-sm">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="gallery-image">
                                                <img class="" src="front/themes/krendels/img/lavki/drov_mini.jpg"
                                                     alt="м. Таганская, Б. Дровяной переулок, д. 8, стр. 2"/>
                                            </div>
                                            <div class="gallery-content">
                                                <div class="gallery-name heading-font">
                                                    <a class="" href="{{url("/pages/taganskaya")}}">м. Таганская</a>
                                                </div>
                                                <div class="gallery-topic">
                                                    м. Таганская, Б. Дровяной переулок, д. 8, стр. 2
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="our-menu-tab tab-pane active p-t-40 p-b-40 bg-white p-l-70 p-r-70" id="kaftab2">

                                <div class="map map-style-1">
                                    <div class="google-map" id="google_map"></div>
                                </div>

                            </div>
                        </div>

                        <!--<div class="our-gallery-all text-center">
                            <button class="au-btn au-btn-pill au-btn-border au-btn-border-invert au-btn-md">Посмотреть все</button>
                        </div>-->
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section Gallery-->


    <section class="our-services our-services-style-1">
        <div class="our-services-wrapper">
                <div class="heading-section text-center p-t-40">
                    <div class="top-heading p-color-1">
                        <span>Наши</span>
                    </div>
                    <div class="main-heading text-uppercase">
                        <h3 class="">Партнеры</h3>
                    </div>
                    <div class="icon-heading">
                        <img src="/front/themes/krendels/favicon.png" width="30" alt="ЛожкиВилки"/>
                    </div>
                </div>
                <!-- End Heading Section-->
                <div class="services-inner m-t-50">
                    <div class="services-list owl-carousel" data-carousel-items="5" data-carousel-lg="3" data-carousel-md="2" data-carousel-sm="1">
                        <div class="services-item services-item-style-1">
                            <div class="services-image">
                                <a class="services-image-hover" href="#!">
                                    <div class="services-item-overlay"></div>
                                    <img src="/front/themes/krendels/img/partners/foodcard_c.png" alt="FoodCard" />
                                </a>
                                <img src="/front/themes/krendels/img/partners/foodcard_g.png" alt="FoodCard" />
                            </div>
                        </div>
                        <!-- End Section Services-->
                        <div class="services-item services-item-style-1">
                            <div class="services-image">
                                <a class="services-image-hover" href="#!">
                                    <div class="services-item-overlay"></div>
                                    <img src="/front/themes/krendels/img/partners/lokobank_c.png" alt="ЛокоБанк" />
                                </a>
                                <img src="/front/themes/krendels/img/partners/lokobank_g.png" alt="ЛокоБанк" />
                            </div>
                        </div>
                        <!-- End Section Services-->
                        <div class="services-item services-item-style-1">
                            <div class="services-image">
                                <a class="services-image-hover" href="#!">
                                    <div class="services-item-overlay"></div>
                                    <img src="/front/themes/krendels/img/partners/sodexo_c.png" alt="Sodexo" />
                                </a>
                                <img src="/front/themes/krendels/img/partners/sodexo_g.png" alt="Sodexo" />
                            </div>
                        </div>
                        <!-- End Section Services-->
                        <div class="services-item services-item-style-1">
                            <div class="services-image">
                                <a class="services-image-hover" href="#!">
                                    <div class="services-item-overlay"></div>
                                    <img src="/front/themes/krendels/img/partners/stng_c.png" alt="Стройтранснефтегаз"/>
                                </a>
                                <img src="/front/themes/krendels/img/partners/stng_g.png" alt="Стройтранснефтегаз"/>
                            </div>
                        </div>
                        <!-- End Section Services-->
                        <div class="services-item services-item-style-1">
                            <div class="services-image">
                                <noindex>
                                    <a class="services-image-hover" href="https://app.halvacard.ru/order/?utm_medium=Partner&utm_source=%7bNAME%7d&utm_campaign=halva"
                                       target="_blank" rel="nofollow" >
                                        <div class="services-item-overlay"></div>
                                        <img src="/front/themes/krendels/img/partners/halva_c.png" alt="Халва"/>
                                    </a>
                                </noindex>
                                <img src="/front/themes/krendels/img/partners/halva_g.png" alt="Халва"/>
                            </div>
                        </div>
                        <!-- End Section Services-->
                    </div>
                    <div class="services-arrow owl-prev services-arrow-left">
                        <i class="fa fa-angle-left"></i>
                    </div>
                    <div class="services-arrow owl-next services-arrow-right">
                        <i class="fa fa-angle-right"></i>
                    </div>
                </div>
            </div>
        </div>
    </section>

        <!-- Адаптивный виджет Instagram -->
        <div class="heading-section text-center">
            <div class="top-heading p-color-1">
                <span>Наш</span>
            </div>
            <div class="main-heading text-uppercase">
                <h3><a href="https://www.instagram.com/dvakrendelya/" title="Instagram 2 кренделя" target="_blank">Instagram</a>
                </h3>
            </div>
            <div class="icon-heading">
                <img src="/front/themes/krendels/favicon.png" width="30" alt="ЛожкиВилки"/>
            </div>
        </div>
        <iframe src="/front/themes/krendels/vendor/inwidget/index.php?adaptive=true&skin=modern-black&toolbar=false"
                data-inwidget scrolling="no" frameborder="no"
                style="border:none;width:100%;height:315px;overflow:hidden;background:none transparent;"
                allowtransparency="true"></iframe>
        {{--<iframe src="//widget.instagramm.ru/?imageW=6&imageH=1&thumbnail_size=122&type=0&typetext=2krendelya&head_show=0&profile_show=0&shadow_show=0&bg=255,255,255,1&opacity=true&head_bg=46729b&subscribe_bg=ad4141&border_color=c3c3c3&head_title=" allowtransparency="true" frameborder="0" scrolling="no" style="border:none;overflow:hidden;width:100%;height:157px;"></iframe>--}}
    </div>
@endsection
@section('scripts')
    <!-- Google Maps-->
    <script src="/front/themes/krendels/js/gmaps.js"></script>
    <script src="/front/themes/krendels/js/gmap2.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyA9Mv1LtIt6Js3Ie_Zg1fa962NRqIo4b6c"></script>
    <!--  sly slider  -->
    <script src="/front/themes/krendels/vendor/sly/dist/sly.js"></script>
    <script src="/front/themes/krendels/js/slymain.js?<?php echo time(); ?>"></script>
@endsection