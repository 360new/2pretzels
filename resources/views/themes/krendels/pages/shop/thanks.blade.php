@extends('themes.krendels.index')
@section('content')
    <div class="page-content shop-single menu-standard-list">
        <div class="page-wrapper">
            <div class="page-inner">
                <div class="container menu-list-wrapper align-center" style="margin-top:100px;">
                    <h1>Спасибо за заказ!</h1>
                    <p class="h3">Ваш заказ успешно принят в обработку! Через 5 секунд вы будете перенаправлены на главную страницу</p>
                    <small>Если этого не произошло, нажмите <a href="{{route('homepage')}}">Здесь</a></small>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        setTimeout(function () {
            location.href = "{{route('homepage')}}";
        }, 5000)
    </script>
@endsection