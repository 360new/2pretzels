@extends('themes.krendels.index')
@section('content')
    <div class="page-content menu-standard-list">
        <section class="heading-page heading-menu-standard-page">
            <div class="heading-page-wrapper small-title-block"
                 style="background: url('/front/themes/krendels/img/cart.jpg') center center no-repeat;">
                <div class="container">
                    <div class="heading-page-inner text-center">
                        <div class="heading-pa  ge-title text-uppercase small-title-block">
                            <!--<h2>Корзина</h2>-->
                        </div>
                        <!--<div class="heading-page-breadcrumb">
                            <ul class="clear-list-style">
                                <li>
                                    <a href="index.html">Домой</a>
                                </li>
                                <li>
                                    <a href="menu-standard-list.html">Корзинка</a>
                                </li>
                            </ul>
                        </div>-->
                    </div>
                </div>
            </div>
        </section>
        <div class="page-wrapper">
            <div class="page-inner">
                <div class="container">
                    <div class="heading-section text-center">
                        <div class="top-heading p-color-1">
                            <span>Корзина</span>
                        </div>
                        <div class="main-heading text-uppercase">
                            <h3>ваш заказ</h3>
                        </div>
                        <div class="icon-heading">
                            <img src="/front/themes/krendels/favicon.png" width="30" alt="2krendels"/>
                        </div>
                    </div>
                    <!-- End Heading Section-->
                    <div class="menu-list menu-list-style-1 cart-menu">
                        {{--<div class="menu-item-wrap">--}}
                        @if($order_items->isEmpty())
                            <div class="menu-list-wrapper clearfix emptycart">
                                <div class="col-md-12 text-center">
                                    <p class="h2">Ваша корзина, к сожалению, пуста.</p>
                                    <p class="h2">Давайте <a href="{{url('/catalog')}}">что-нибудь закажем</a></p>
                                </div>
                                @else
                                    <div class="menu-list-wrapper clearfix">
                                        @foreach($order_items as $product)
                                            <div class="menu-item product-item p-t-15 p-b-15 clearfix"
                                                 data-product-id="{{$product->id}}">
                                                <div class="dish-main clearfix">
                                                    <a class="dish-image" href="{{$product->categories->first()->getUrl() .'/'. $product->slug}}">
                                                        @if(is_object($product->miniature()))
                                                            <img class=""
                                                                 src="{{$product->miniature()->thumbnail}}"
                                                                 onerror="this.src='/front/themes/krendels/img/placeholder.jpg'"
                                                                 alt="{{$product->title}}"/>
                                                        @else
                                                            <img class=""
                                                                 src="/front/themes/krendels/img/placeholder_big.jpg"
                                                                 alt="{{$product->title}}"/>

                                                        @endif
                                                    </a>
                                                    <div class="dish-content">
                                                        <div class="dish-name text-uppercase">
                                                            <a href="{{$product->categories->first()->getUrl() .'/'. $product->slug}}"
                                                               class="heading-font">{{$product->name}}</a>

                                                        </div>
                                                        <div class="dish-topic">
                                                            <a href="{{$product->categories->first()->getUrl()}}">{{$product->categories->first()->name}}</a>
                                                        </div>
                                                    </div>
                                                    <div class="dish-cost">
                                                        @if($product->sold == 0)
                                                            <span>{{$product->price}} ₽</span>
                                                        @else
                                                            <span>--- ₽</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                @if($product->sold == 0)
                                                    <div class="dish-control">
                                                        <div class="dish-control-del"></div>
                                                        <div class="dish-control-empty"></div>
                                                        <div class="dish-control-minus"></div>
                                                        <input class="dish-control-input" maxlength="3" max="999" min="0"
                                                               type="number" size="3"
                                                               data-prodmin="{{$product->minimum}}"
                                                               value="{{$product->quantity}}">
                                                        <div class="dish-control-plus"></div>
                                                        {{--<div class="dish-control-cart"></div>--}}
                                                        <button class="dish-control-cart" type="submit">Изменить
                                                        </button>
                                                        @else
                                                            <div class="dish-control dish-control-sold">
                                                                <div class="dish-control-empty text-right">Товар
                                                                    недоступен
                                                                </div>
                                                                <div class="dish-control-minus"></div>
                                                                <input class="dish-control-input" maxlength="3"
                                                                       max="999" min="0" type="number" size="3"
                                                                       data-prodmin="{{$product->minimum}}" value="0">
                                                                <div class="dish-control-plus"></div>
                                                                <button class="dish-control-cart" type="submit">
                                                                    УДАЛИТЬ
                                                                </button>
                                                                @endif

                                                            </div>
                                                    </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                            <div class="menu-list-summary">
                                                <span class="menu-list-summary-info"></span>
                                                <span class="menu-list-summary-txt">Итого:</span> <span
                                                        class="menu-list-summary-summ">1000</span>₽
                                            </div>
                                    </div>
                            </div>
                            <section class="booking booking-style-1 booking-style-3 clearfix">
                                <div class="booking-wrapper">
                                    <div class="booking-inner">
                                        <div class="box-30 matchHeight">
                                            <div class="time-open-wrapper bg-dark-brown p-30">
                                                <div class="time-open-inner text-center p-t-30 p-b-20">
                                                    <div class="heading-section text-center">
                                                        <div class="top-heading p-color-1">
                                                            <span>Наши</span>
                                                        </div>
                                                        <div class="main-heading text-uppercase">
                                                            <h3 class="text-white">кафе</h3>
                                                        </div>
                                                        <div class="icon-heading">
                                                            <img src="/front/themes/krendels/img/icons/ic-plate.png"
                                                                 alt="plate"/>
                                                        </div>
                                                    </div>

                                                    <!-- End Heading Section-->
                                                    <div class="time-open-contain m-t-25">
                                                        <div class="time-open-p-h p-l-10 p-r-10">
                                                            <h4 class="heading-font">Подробнее о местоположении наших кафе</h4>
                                                        </div>
                                                        
                                                        <div class="time-open-phone p-t-15" style="width: 100%">
                                                            <p class="heading-font text-left p-l-10">
                                                                <a style="color: #af7746;"
                                                                   href="{{url("/pages/restaurants/aeroport")}}">
                                                                    Аэропорт</a>
                                                            </p>
                                                        </div>
                                                        <div class="time-open-phone p-t-15" style="width: 100%">
                                                            <p class="heading-font text-left p-l-10">
                                                                <a style="color: #af7746;"
                                                                   href="{{url("/pages/restaurants/kr-vorota")}}">
                                                                    Красные
                                                                    ворота</a>
                                                            </p>
                                                        </div>
                                                        <div class="time-open-phone p-t-15" style="width: 100%">
                                                            <p class="heading-font text-left p-l-10">
                                                                <a style="color: #af7746;"
                                                                   href="{{url("/pages/restaurants/technopark")}}">
                                                                    Технопарк</a>
                                                            </p>
                                                        </div>
                                                        <div class="time-open-phone p-t-15" style="width: 100%">
                                                            <p class="heading-font text-left p-l-10">
                                                                <a style="color: #af7746;"
                                                                   href="{{url("/pages/restaurants/trubnaya")}}">
                                                                    Трубная</a>
                                                            </p>
                                                        </div>
                                                        <div class="time-open-phone p-t-15" style="width: 100%">
                                                            <p class="heading-font text-left p-l-10">
                                                                <a style="color: #af7746;"
                                                                   href="{{url("/pages/restaurants/park-kultury")}}">
                                                                    Парк
                                                                    культуры</a>
                                                            </p>
                                                        </div>
                                                        <div class="time-open-phone p-t-15" style="width: 100%">
                                                            <p class="heading-font text-left p-l-10">
                                                                <a style="color: #af7746;"
                                                                   href="{{url("/pages/restaurants/taganskaya")}}">
                                                                    Таганская</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-70 matchHeight">
                                            <div class="booking-form-wrapper p-t-100 p-b-10 p-r-35 p-l-35">
                                                <div class="booking-form-inner">
                                                    <div class="heading-section text-center">
                                                        <div class="top-heading p-color-1">
                                                            <span>Заказ</span>
                                                        </div>
                                                        <div class="main-heading text-uppercase">
                                                            <h3>онлайн</h3>
                                                        </div>
                                                        <div class="icon-heading">
                                                            <img src="/front/themes/krendels/favicon.png" width="30"
                                                                 alt="2krendels"/>
                                                        </div>
                                                    </div>
                                                    <!-- End Heading Section-->
                                                    <div class="booking-input">
                                                        <form class="booking-form" method="POST"
                                                              action="{{route('cart.confirm')}}">
                                                            <div class="booking-input-group clearfix m-b-30 m-t-50">
                                                                <div class="deliver_checkbox_wrap">
                                                                    <label class="deliver_checkbox">
                                                                        <input class="deliver_checkbox_input" type="checkbox" name="deliverornot"/>
                                                                        <span></span>
                                                                        Без доставки / Забрать самому
                                                                    </label>
                                                                </div>
                                                                <div class="booking-input-item p-r-15 p-l-15">
                                                                    <input class="au-input validate" placeholder="ФИО"
                                                                           name="client"
                                                                           value="{{Auth::check()? Auth::user()->name: ''}}">
                                                                </div>
                                                                <div class="booking-input-item p-r-15 p-l-15">
                                                                    <input class="au-input booking-adress-home validate" name="address"
                                                                           placeholder="Адрес"/>
                                                                    <select class="au-select booking-adress-take validate2" name="address">
                                                                        <option  disabled selected data-display="" data-address="">Адрес получения</option>
                                                                        <option value="Лавка 2 Кренделя на м.Аэропорт" data-address="Лавка 2 Кренделя на м.Аэропорт">Аэропорт</option>
                                                                        <option value="Лавка 2 Кренделя на м.Красные ворота" data-address="Лавка 2 Кренделя на м.Красные ворота">Красные ворота</option>
                                                                        <option value="Лавка 2 Кренделя на м.Технопарк" data-address="Лавка 2 Кренделя на м.Технопарк">Технопарк</option>
                                                                        <option value="Лавка 2 Кренделя на м.Трубная" data-address="Лавка 2 Кренделя на м.Трубная">Трубная</option>
                                                                        <option value="Лавка 2 Кренделя на м.Парк культуры" data-address="Лавка 2 Кренделя на м.Парк культуры">Парк культуры</option>
                                                                        <option value="Лавка 2 Кренделя на м.Таганская" data-address="Лавка 2 Кренделя на м.Таганская">Таганская</option>
                                                                    </select>
                                                                </div>
                                                                <div class="booking-input-item p-l-15 p-r-15">
                                                                    <input type="email" class="au-input validate cart-email" name="email"
                                                                           placeholder="email"/>
                                                                </div>
                                                            </div>
                                                            <div class="booking-input-group clearfix m-b-50">
                                                                <div class="booking-input-item p-r-15 p-l-15">
                                                                    <input class="au-input validate cart-phone" name="phone"
                                                                           placeholder="Телефон"/>
                                                                </div>
                                                                <div class="booking-input-item p-r-15 p-l-15">
                                                                    <select class="au-select booking-input-item-deliver validate2" name="payment">
                                                                        <option disabled selected data-display="">Способ оплаты</option>
                                                                        <option value="card">Онлайн банковской картой
                                                                        </option>
                                                                        <option value="invoice">Безналичный расчет
                                                                        </option>
                                                                        <option value="cash">Наличными курьеру</option>
                                                                    </select>
                                                                    <select class="au-select booking-input-item-nodeliver validate2" name="payment">
                                                                        <option disabled selected data-display="">Способ оплаты</option>
                                                                        <option value="card">Онлайн банковской картой
                                                                        </option>
                                                                        <option value="invoice">Безналичный расчет
                                                                        </option>
                                                                        <option value="cash">Наличными в кафе</option>
                                                                    </select>
                                                                </div>
                                                                <div class="booking-input-item p-l-15 p-r-15 au-input-icon clock">
                                                                    <input class="au-select-time au-input validate" name="time"
                                                                           placeholder="Время доставки"/>
                                                                </div>
                                                                <div class="booking-input-item p-l-15 p-r-15 m-t-20">
                                                                    <input type="number" class="au-input validate cart-email" name="persons"
                                                                           placeholder="Количество персон" min="1" required>
                                                                </div>
                                                            </div>
                                                            <div class="booking-personal">
                                                                <div class="booking-personal_checkbox_wrap">
                                                                    <label class="booking-personal_checkbox">
                                                                        <input class="validate" type="checkbox" name="allowpersonal"/>
                                                                        <span></span>
                                                                        Согласен/Согласна на обработку моих персональных данных.
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="booking-submit text-center">
                                                                <button class="text-uppercase au-btn-lg au-btn au-btn-pill au-btn-black ripplelink confirm-client-order"
                                                                        disabled="disabled"
                                                                        type="submit">Отправить заказ!
                                                                </button>
                                                            </div>
                                                            <div class="text-left p-l-15 p-r-15 m-t-30 cart-address-nodelivery">
                                                                Адрес получения: <span class="cart-address-nodelivery-adr"></span>
                                                            </div>
                                                            {{ csrf_field() }}
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- End Section Booking-->
                    </div>
                </div>
            </div>
            <!--<div class="map map-style-1">
                <div class="google-map" id="google_map" data-map-x="37.3863866" data-map-y="-122.0576091" data-pin="img/icons/ic-cricle-02.png" data-scrollwhell="0" data-draggable="1"></div>
            </div>-->
            <!-- End Map-->
        </div>

@endsection
@section('scripts')
<script src="front/themes/krendels/js/validate.js"></script>
<script src="https://unpkg.com/imask"></script>
<script>
    //masks
    var phoneMask = new IMask(
        $('.cart-phone')[0], {
            mask: '+{7}(000)000-00-00'
        });
</script>
@endsection