@extends('themes.krendels.index')
@section('content')
    <div class="page-content menu-standard-list shop-list-03">
        @component('themes.krendels.components.page_heading',[
      'background'       => $background,
      'title'            => $content->name,
      'need_breadcrumbs' => true,
      'elements'=> $elements
      ])@endcomponent
        <div class="page-wrapper">
            <div class="page-inner">
                <div class="container">
                    <div class="row">
                        @if(!$content->children->isEmpty())
                            <div class="col-md-9">
                                @else
                                    <div class="col-md-12">
                                    @endif
                                <div class="shop-dish-list">
                                    @if(!empty($content->caption) && (!isset(request()->page) || request()->page == 1))
                                        <div class="row" style="margin-bottom:50px;">
                                            <div class="menu-list-wrapper">
                                                {!!$content->caption!!}
                                            </div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        @if($products->isEmpty() && !$content->children->isEmpty())

                                             @foreach($content->children->where('status','public') as $child)
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="shop-item matchHeight product-item">
                                                        <div class="shop-item-image">
                                                            <a href="{{$child->getUrl()}}">
                                                            @if(is_object($child->miniature()))
                                                                <img class="img-responsive"
                                                                     src="{{$child->miniature()->medium}}"
                                                                     onerror="this.src='/front/themes/krendels/img/placeholder.jpg'"
                                                                     alt="{{$child->title}}"/>
                                                            @else
                                                                <img class="img-responsive"
                                                                     src="/front/themes/krendels/img/placeholder.jpg"
                                                                     alt="{{$child->title}}"/>

                                                            @endif
                                                            </a>
                                                        </div>
                                                        <div class="shop-item-name">
                                                            <a href="{{$child->getUrl()}}">{{$child->name}}</a>
                                                        </div>

                                                    </div>
                                                </div>
                                                @endforeach
                                        @else
                                        @foreach($products as $product)
                                            @if(!empty($content->caption) && (!isset(request()->page) || request()->page == 1))
                                            <div class="col-md-4 col-sm-6">
                                            @else
                                                <div class="col-md-4 col-sm-6">
                                            @endif
                                                <div class="shop-item matchHeight product-item clearfix @if(!is_null($cart) && array_key_exists ($product->id, $cart)) cartin @endif"
                                                     data-product-id="{{$product->id}}">
                                                    <div class="shop-item-image">
                                                        <a href="{{$product->categories->where('type','category')->first()->getUrl() . '/' . $product->slug}}">
                                                        @if(is_object($product->miniature()))
                                                            <img class="img-responsive"
                                                                 src="{{$product->miniature()->medium}}?v=1"
                                                                 onerror="this.src='/front/themes/krendels/img/placeholder.jpg'"
                                                                 alt="{{$product->name}}"/>
                                                        @else
                                                            <img class="img-responsive"
                                                                 src="/front/themes/krendels/img/placeholder.jpg"
                                                                 alt="{{$product->name}}"/>
                                                        @endif
                                                        </a>
                                                    <!--<a class="au-btn au-btn-black au-btn-pill au-btn-lg" href="#">В корзину</a>-->
                                                    </div>
                                                    <div class="shop-item-name">
                                                        <a href="{{$product->categories->where('type','category')->first()->getUrl() . '/' . $product->slug}}">{{$product->name}}</a>
                                                    </div>
                                                    <div class="shop-item-cost">
                                                        <p>{{$product->price}}₽ / {{$product->weight}}</p>
                                                    </div>

                                                    @if($product->sold == 0)
                                                        <div class="dish-control">
                                                            <div class="dish-control-del"></div>
                                                            <div class="dish-control-empty"></div>
                                                    @else
                                                        <div class="dish-control dish-control-sold">
                                                            <div class="dish-control-empty text-right">Товар недоступен</div>
                                                    @endif
                                                        <div class="dish-control-minus"></div>
                                                        <input class="dish-control-input" maxlength="3"  max="999" min="0"  type="number" size="3"  data-prodmin="{{$product->minimum}}" @if(!is_null($cart) && array_key_exists ($product->id, $cart))
                                                        value="{{$cart[$product->id]}}"
                                                               @else
                                                               value="0"
                                                                @endif >
                                                        <div class="dish-control-plus"></div>
                                                        <div class="dish-control-cart4"></div>
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach
                                                @endif

                                    </div>
                                </div>
                                <div class="shop-list-footer page-control-widget">
                                    {!! $products->links('themes.krendels.components.pagination') !!}
                                </div>
                                <!-- End Section Shop List-->
                            </div>
                            @if(!$content->children->isEmpty())
                                <div class="col-md-3">
                                    {{--<div class="search-widget">
                                        <form action="">
                                        <div class="title-widget">
                                            <h4>Поиск</h4>
                                        </div>
                                        <div class="search-form">
                                            <input type="text"/>
                                            <a href="#">
                                                <img src="/front/themes/krendels/img/icons/ic-search.png" alt="Поиск"/>
                                            </a>
                                        </div>

                                        <div class="shop-filter">
                                            <select class="au-select">
                                                <option data-display="">Категории</option>
                                                <option value="1">Постные блюда</option>
                                                <option value="2">Фуршеты, наборы, канапе</option>
                                                <option value="3">Готовые наборы</option>
                                                <option value="4">Кофе-брейк наборы</option>
                                                <option value="0">Домашний банкет</option>
                                            </select>
                                        </div>
                                        </form>
                                    </div>--}}
                                    <div class="categories-widget">
                                        <div class="title-widget">
                                            <h4>Подкатегории</h4>
                                        </div>
                                        <ul>
                                            <li>
                                                @foreach($content->children->where('status', 'public') as $child)
                                                    <a href="{{$child->getUrl()}}">
                                                        <span>{{$child->name}}</span>
                                                    </a>
                                                @endforeach
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tags-widget">
                                        <div class="title-widget">
                                            <h4>Метки</h4>
                                        </div>
                                        <div class="tags-list">
                                            @foreach($tags as $tag)
                                            <div class="tag-item">
                                                <a href="{{url('/catalog/tags/'.$tag->slug)}}">{{$tag->name}}</a>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection