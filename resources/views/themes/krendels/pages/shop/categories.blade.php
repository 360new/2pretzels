@extends('themes.krendels.index')
@section('content')
    <div class="page-content menu-standard-list shop-list-03">
        @component('themes.krendels.components.page_heading',[
      'background'       => $background,
      'title'            => $content->name,
      'need_breadcrumbs' => true,
      'elements'=> $elements
      ])@endcomponent
        <div class="page-wrapper">
            <div class="page-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="shop-dish-list">
                                <div class="row">
                                    @foreach($content as $category)
                                        <div class="col-md-3 col-sm-6">
                                            <div class="shop-item matchHeight product-item">
                                                <div class="shop-item-image">
                                                    <a href="{{$category->getUrl()}}">
                                                        @if(is_object($category->miniature()))
                                                            <img class="img-responsive" src="{{$category->miniature()->medium}}" alt="{{$category->title}}"/>
                                                        @else
                                                            <img class="img-responsive" src="/front/themes/krendels/img/placeholder.jpg" alt="{{$category->title}}"/>
                                                        @endif
                                                    </a>
                                                </div>
                                                <div class="shop-item-name">
                                                    <a href="{{$category->getUrl()}}">{{$category->name}}</a>
                                                </div>

                                            </div>
                                        </div>

                                    @endforeach

                                </div>
                            </div>
                            <!-- End Section Shop List-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection