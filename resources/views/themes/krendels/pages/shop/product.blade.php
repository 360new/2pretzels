@extends('themes.krendels.index')
@section('styles')
    <link href="/front/themes/krendels/vendor/slick/slick.css" rel="stylesheet">
    <link href="/front/themes/krendels/vendor/slick/slick-theme.css" rel="stylesheet">
@endsection
@section('content')
    <div class="page-content shop-single menu-standard-list">
        @component('themes.krendels.components.page_heading',[
      'background'       => $background,
      'title'            => $content->name,
      'need_breadcrumbs' => true,
      'elements'=> $elements]) @endcomponent
        <div class="page-wrapper">
            <div class="page-inner">
                <div class="container">
                    <div class="shop-single">
                        <div class="row">
                            <div class="col-lg-6">
                                @if(!$content->gallery()->isEmpty())
                                    <div class="post-image">
                                        <div class="post-image-main">
                                            <div class="thumb-main">
                                                @foreach($content->gallery() as $gal)
                                                    @if($loop->first)
                                                        <div class="thumb-main-item">
                                                            @else
                                                                <div class="thumb">
                                                                    @endif
                                                                    <img 
                                                                         src="{{$gal->full}}"
                                                                         onerror="this.src='/front/themes/krendels/img/placeholder_big.jpg'"
                                                                         alt="{{$content->name}}">
                                                                </div>
                                                                @endforeach
                                                        </div>
                                            </div>
                                        </div>
                                        @if (count($content->gallery()) > 1)
                                            <div class="post-image-thumb">
                                                <div class="thumb-list1">
                                                    @foreach($content->gallery() as $gal)
                                                        <div class="thumb">
                                                            <img class=""
                                                                 src="{{$gal->thumbnail}}"
                                                                 onerror="this.src='/front/themes/krendels/img/placeholder.jpg'"
                                                                 alt="{{$content->name}}">
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @else
                                    <div class="post-image">
                                        <div class="post-image-main">
                                            <div class="thumb-main">
                                                <div class="thumb-main-item">
                                                    <img class=""
                                                         @if(is_object($content->miniature()))
                                                         src="{{$content->miniature()->large}}"
                                                         @else
                                                         src="/front/themes/krendels/img/placeholder_big.jpg"
                                                         @endif
                                                         onerror="this.src='/front/themes/krendels/img/placeholder_big.jpg'"
                                                         alt="{{$content->name}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            @endif
                            <div class="col-lg-6">

                                <div class="post-content product-item  @if(!is_null($cart) && array_key_exists ($content->id, $cart)) cartin
                                @endif" data-product-id="{{$content->id}}">
                                    <div class="dish-title">
                                        <h3>{{$content->name}}</h3>
                                    </div>
                                    <div class="dish-cost">
                                        <span>{{$content->price}} ₽</span>
                                    </div>
                                    {!! $content->short_product_description !!}
                                    @if($content->sold == 0)
                                        <div class="dish-control">
                                            <div class="dish-control-del"></div>
                                            <div class="dish-control-empty"></div>
                                            @else
                                                <div class="dish-control dish-control-sold">
                                                    <div class="dish-control-empty text-right">Товар недоступен</div>
                                                    @endif
                                                    <div class="dish-control-minus"></div>
                                                    <input class="dish-control-input" maxlength="3" max="999" min="0"
                                                           type="number"
                                                           size="3" data-prodmin="{{$content->minimum}}"
                                                           @if(!is_null($cart) && array_key_exists ($content->id, $cart)) value="{{$cart[$content->id]}}"
                                                           @else value="0" @endif>
                                                    <div class="dish-control-plus"></div>
                                                    <div class="dish-control-cart4"></div>
                                                </div>
                                        </div>

                                </div>
                            </div>
                        </div>
                        <div class="shop-accordion m-t-100 m-b-100">
                            <div data-accordion-group="data-accordion-group">
                                @if(!is_null($content->product_description))
                                    <div class="accordion open" data-accordion="data-accordion">
                                        <div class="accordion-title" data-control="data-control">
                                            <h4>Описание</h4>
                                        </div>
                                        <div class="accordion-content" data-content="data-content">
                                            {!! $content->product_description !!}
                                        </div>
                                    </div>
                                @endif

                                @if(!is_null($content->additonal_description))
                                    <div class="accordion" data-accordion="data-accordion">
                                        <div class="accordion-title" data-control="data-control">
                                            <h4>Дополнительная информация</h4>
                                        </div>
                                        <div class="accordion-content" data-content="data-content">
                                            {!! $content->additonal_description !!}
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                    <!-- End Shop Single-->
                    <div class="related-products">
                        <div class="title-widget text-center">
                            <h4>Вместе с этим часто берут</h4>
                        </div>
                        <div class="products-list">
                            <div class="owl-carousel" data-carousel-items="4" data-carousel-lg="3" data-carousel-md="2"
                                 data-carousel-sm="1" data-carousel-margin="30">
                                @foreach($content->categories->first()->products->where('status', 'public')->take(10) as $product)
                                    <div class="shop-item">
                                        <div class="shop-item-image">
                                            @if(is_object($product->miniature()))
                                                <img class=""
                                                     src="{{$product->miniature()->small}}"
                                                     onerror="this.src='/front/themes/krendels/img/placeholder.jpg'"
                                                     alt="{{$product->title}}"/>
                                            @else
                                                <img class=""
                                                     src="/front/themes/krendels/img/placeholder.jpg"
                                                     alt="{{$product->title}}"/>

                                            @endif
                                        </div>
                                        <div class="shop-item-name">
                                            <a href="{{$product->categories->first()->slug . '/' . $product->slug}}">{{$product->name}}</a>
                                        </div>
                                        <div class="shop-item-cost">
                                            <p>{{$product->price}} ₽ @if($product->weight)
                                                    / {{$product->weight}}@endif</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="arrow-right owl-next">
                                <i class="fa fa-angle-right"></i>
                            </div>
                            <div class="arrow-left owl-prev">
                                <i class="fa fa-angle-left"></i>
                            </div>
                        </div>
                    </div>
                    <!-- End related-products-->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/front/themes/krendels/vendor/slick/slick.min.js"></script>
    <script>
        $('.thumb-list1').slick({
            infinite: true,
            slidesToShow: 1,
            centerMode: true,
            autoplay: false,
            arrows: true,
            variableWidth: true,
            asNavFor: '.thumb-main',
            focusOnSelect: true
        });
        $('.thumb-main').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            centerMode: true,
            asNavFor: '.thumb-list1'
        });
    </script>
@endsection