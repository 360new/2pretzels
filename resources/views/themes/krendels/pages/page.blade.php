@extends('themes.krendels.index')
@section('styles')
    <link href="/front/themes/krendels/vendor/slick/slick.css" rel="stylesheet">
    <link href="/front/themes/krendels/vendor/slick/slick-theme.css" rel="stylesheet">
@endsection
@section('content')
    <div class="page-content shop-single menu-standard-list">
        <section class="heading-page heading-menu-standard-page">
            <div class="heading-page-wrapper"
                 @if(is_object($post->miniature()))
                 style="background: url('{{$post->miniature()->large}}') center center no-repeat;"
                 @else
                 style="background: url('/front/themes/krendels/img/bg-comming-soon.jpg') center center no-repeat;"
                    @endif
            >
                <div class="container">
                    <div class="heading-page-inner text-center">
                        <div class="heading-page-title text-uppercase">
                            <h2>{{$post->title}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="page-wrapper">
            <div class="page-inner page-single-inner">
                <div class="container">
                    {!! $post->content !!}
                </div>
                @if(!$post->gallery()->isEmpty())
                    <div class="container">
                        <div class="post-image">
                            <div class="m-b-20"></div>
                            <div class="post-image-main">
                                <div class="thumb-main">
                                    @foreach($post->gallery() as $gal)
                                        @if($loop->first)
                                            <div class="thumb-main-item">
                                                @else
                                                    <div class="thumb">
                                                        @endif
                                                        <img
                                                             src="{{$gal->full}}"
                                                             alt="{{$post->title}}">
                                                    </div>
                                                    @endforeach
                                            </div>
                                </div>
                                <div class="arrow-right owl-next">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                                <div class="arrow-left owl-prev">
                                    <i class="fa fa-angle-left"></i>
                                </div>
                            </div>
                            <div class="post-image-thumb">
                                <div class="thumb-list1">
                                    @foreach($post->gallery() as $gal)
                                        <div class="thumb">
                                            <img class="img-responsive" src="{{$gal->thumbnail}}"
                                                 alt="{{$post->title}}">
                                        </div>
                                    @endforeach
                                </div>
                                <div class="arrow-right owl-next">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                                <div class="arrow-left owl-prev">
                                    <i class="fa fa-angle-left"></i>
                                </div>
                            </div>

                        </div>
                    </div>
                @endif
                <div class="container">
                    {!! $post->additional !!}
                </div>
            </div>

        </div>
        @endsection
        @section('scripts')
            <script src="/front/themes/krendels/vendor/slick/slick.min.js"></script>

            <script>
                $('.thumb-list1').slick({
                    infinite: true,
                    slidesToShow: 1,
                    centerMode: true,
                    autoplay: false,
                    arrows: true,
                    variableWidth: true,
                    asNavFor: '.thumb-main',
                    focusOnSelect: true
                });
                $('.thumb-main').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    fade: true,
                    centerMode: true,
                    prevArrow:'.arrow-left',
                    nextArrow:'.arrow-right',
                    asNavFor: '.thumb-list1'
                });
            </script>
@endsection