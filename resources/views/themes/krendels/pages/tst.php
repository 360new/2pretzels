@foreach(\App\Models\Shop\ProductCategory::where('parent_id', '<>', null)->get() as $category)
    <div class="container our-menu-tab tab-pane @if($loop->first) active @endif p-t-40 p-b-40 bg-white p-l-40 p-r-40" id="bltab{{$category->id}}">
        <div class="our-menu-tab-inner"> <!-- Добавляем класс .owl-carousel, и карусель работает -->
            <div class="owl-carousel-item menu-item-wrap">
                @foreach($category->products->take(6) as $item)

                <div class="menu-item p-t-15 p-b-15 clearfix" data-product-id="{{$item->id}}">
                    <div class="dish-main clearfix">
                        <div class="dish-image">
                            <img src="/front/themes/krendels/img/dish-item-01.jpg" alt="#" />
                        </div>
                        <div class="dish-content">
                            <div class="dish-name text-uppercase">
                                <a href="{{$item->categories->first()->getUrl() .'/'. $item->slug}}" class="heading-font">{{$item->name}}</a>
                            </div>
                            <div class="dish-topic">
                                <a href="#!">Мясные супы</a>
                            </div>
                        </div>
                        <div class="dish-cost">
                            <span>{{$item->price}} ₽</span>
                        </div>

                    </div>
                    <div class="dish-control">
                        <div class="dish-control-empty"></div>
                        <div class="dish-control-minus"></div>
                        <input class="dish-control-input" maxlength="3"  max="999" min="0"  type="number" size="3" >
                        {{--@if(!is_null($cart) && array_key_exists ($item->id, $cart)) value="{{$cart[$item->id]}}"  @else value="0" @endif--}}

                        <div class="dish-control-plus"></div>
                        <div class="dish-control-cart"></div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
        <!--<div class="owl-carousel-item">
          <тут еще один слайд карусели></тут>
        </div>-->

        <!--<div class="tab-arrow-right owl-next">
            <i class="fa fa-angle-right"></i>
        </div>
        <div class="tab-arrow-left owl-prev">
            <i class="fa fa-angle-left"></i>
        </div>-->
    </div>
@endforeach