@extends('themes.krendels.index')
@section('content')
    <div class="page-content menu-standard-list shop-list-03">
        @component('themes.krendels.components.page_heading',[
      'background'       => '/front/themes/krendels/img/bg-comming-soon.jpg',
      'title'            => 'Личный кабинет'
      ])@endcomponent
        <div class="page-wrapper">
            <div class="page-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div>
                                <p class="h4">Миниатюра</p>
                                @if(is_object(Auth::user()->miniature()))
                                    <img src="{{Auth::user()->miniature()->small}}" alt="" class="image-responsive mb-15" width="100%">
                                @endif
                                <input type="file" name="image" form="form-data">
                                <button class="btn btn-primary" type="submit" form="form-data">
                                    Сохранить настройки
                                </button>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <form name="form-data" id="form-data" enctype="multipart/form-data"
                                  action="{{route('users.update', Auth::user()->id)}}"
                                  method="POST">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Имя пользователя</label>
                                    <input type="text" class="form-control" value="{{Auth::user()->name}}"
                                           name="name"
                                           placeholder="Новый пользователь">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Email</label>
                                    <input type="email" class="form-control" value="{{Auth::user()->email}}"
                                           name="email"
                                           placeholder="user@example.com">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Пароль</label>
                                    <input type="password" class="form-control"
                                           name="password">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Подтверждение
                                        пароля</label>
                                    <input type="password" class="form-control"
                                           name="password_repeat">
                                </div>
                                <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                @method('PUT')
                                @csrf
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-center"><h2>Заказы</h2></div>
                        <div class="col-xs-12">
                            <table class="table table-hover">
                                <tr>
                                    <th>Номер</th>
                                    <th>Содержимое заказа</th>
                                    <th>Информация о заказе</th>
                                </tr>
                                @if(Auth::user()->orders->where('status', '<>', 'cart')->isEmpty())
                                <tr>
                                    <td colspan="3">Пока что вы не оформили ни одного заказа</td>
                                </tr>
                                @else
                                @foreach(Auth::user()->orders->where('status', '<>', 'cart') as $order)
                                    <tr>
                                        <td>
                                            {{$order->id}}
                                        </td>
                                        <td>
                                            {{$order->getReadableContent()}}
                                        </td>
                                        <td>
                                            Доставить к: {{$order->time}} |
                                            Оплата: {{\App\Models\Shop\Order::PAYMENTS[$order->payment]}} |
                                            Статус: {{\App\Models\Shop\Order::STATUSES[$order->status]}}<br>
                                            Контакты: {{$order->phone}} | {{$order->email}}<br>
                                            Адрес: {{$order->address}}
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@endsection