<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">

    <title>{{isset($title) ? $title : \App\Models\Settings::first()->site_name}}</title>
    <meta name="description"
          content="{{isset($description) ? $description : \App\Models\Settings::first()->default_description}}">
    <meta name="keywords" content="{{isset($keywords) ? $keywords : \App\Models\Settings::first()->default_keywords}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/front/themes/krendels/favicon.png" type="image/png" sizes="32x32"> <!-- sizes="16x16" -->
    <!--Fonts-->
    <link href="/front/themes/krendels/fonts/fonts.css" rel="stylesheet">
    <!--Icons fonts-->
    <link href="/front/themes/krendels/fonts/font-awesome.min.css" rel="stylesheet">
    <!--Styles-->
    <link href="/front/themes/krendels/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/front/themes/krendels/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="/front/themes/krendels/vendor/css-hamburgers/dist/hamburgers.min.css" rel="stylesheet">
    <!-- Revolution Slider -->
    <link href="/front/themes/krendels/vendor/revolution/css/layers.css" rel="stylesheet">
    <link href="/front/themes/krendels/vendor/revolution/css/navigation.css" rel="stylesheet">
    <link href="/front/themes/krendels/vendor/revolution/css/settings.css" rel="stylesheet">
    <!-- Select and date time picker-->
    <link href="/front/themes/krendels/vendor/jquery-nice-select/css/nice-select.css" rel="stylesheet">
    <link href="/front/themes/krendels/vendor/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
    <!-- Owl carousel-->
    <link href="/front/themes/krendels/vendor/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/front/themes/krendels/vendor/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">

    <!-- Основной стиль -->
    <link href="{{asset('css/main.css')}}?<?php echo time(); ?>" rel="stylesheet" type="text/css">
    @yield('styles')

    <meta name="yandex-verification" content="d7924c66aa1df16f" />
    <meta name="google-site-verification" content="8pPr8t_1ovcHc8UZvPGKba1uy10a8ODyO3Hj52AM4eU" />
</head>

<body>
    <div class="modal fade" id="modal-info">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">УСЛОВИЯ</h4>
                </div>
                <div class="modal-body">
                        <span>Минимальная сумма заказа 4000р.</span>
                        <span>Оформление заказа не позднее 48 часов.</span>
                        <span>Заказ оформленный после 15:00, примается в работу в 10:00 следующего дня</span>
                        <span>Зона доставки — в пределах МКАД</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <a class="btn btn-primary" href="{{route('cart')}}">Перейти в корзину</a>
                </div>
            </div>
        </div>
    </div>
<header>
        <div class="header header-style-1">
            <div class="header-wrapper bg-transparent">
                <div class="header-inner">
                    <input type="hidden" class="csrf-field" value="{{csrf_token()}}">
                    <div class="container-fluid">
                        <a href="{{route('homepage')}}" title="На главную">
                            <div class="logo">
                                    <span>Два кренделя</span>
                                    <!--<img src="/front/themes/krendels/img/2kr_light.png" width="180">-->
                            </div>
                        </a>
                        <a class="hamburger hamburger--collapse" href="#menu-mobile" id="toggle-icon">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </a>
                        <a class="shopping-cart-mobile" href="#info" data-toggle="modal" data-target="#modal-info">
                            <div class="shopping-cart-mobile-uppertext1">заказ за 48ч</div>
                            <div class="shopping-cart-mobile-uppertext2">от 4000₽</div>
                            <svg version="1.1" id="Capa_1" class="shopping-cart-img"
                                 xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="28px" height="28px" viewBox="0 0 510 510"
                                 style="enable-background:new 0 0 510 510;" xml:space="preserve">
                                <g>
                                    <g id="shopping-cart-path">
                                        <path d="M153,408c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51S181.05,408,153,408z M0,0v51h51l91.8,193.8L107.1,306
                                            c-2.55,7.65-5.1,17.85-5.1,25.5c0,28.05,22.95,51,51,51h306v-51H163.2c-2.55,0-5.1-2.55-5.1-5.1v-2.551l22.95-43.35h188.7
                                            c20.4,0,35.7-10.2,43.35-25.5L504.9,89.25c5.1-5.1,5.1-7.65,5.1-12.75c0-15.3-10.2-25.5-25.5-25.5H107.1L84.15,0H0z M408,408
                                            c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51S436.05,408,408,408z"/>
                                    </g>
                                </g>
                            </svg>
                            <span class="badge badge-red cart-items-badge">0</span>
                            <div class="shopping-cart-mobile-undertext">0₽</div>
                        </a>
                        <nav class="navbar-menu">
                            <ul class="menu-lists clear-list-style text-uppercase">

                                <li class="dropdown">
                                    <!--<i class="fa fa-angle-down"></i>-->
                                    <span  class="dropdown-caption">Меню для мероприятий</span>
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">
                                        <li  class="dropdown">
                                            <span class="dropdown-caption">Фуршет</span>
                                            {{--<a href="{{url('/catalog/furshet')}}">Фуршет</a>--}}
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{url('/catalog/furshet/furshet-zakuski')}}">Закуски</a>
                                                </li>
                                                <li>
                                                    <a href="{{url('/catalog/furshet/furshet-goryachie-blyuda')}}">Горячие блюда</a>
                                                </li>
                                                <li>
                                                    <a href="{{url('/catalog/furshet/furshet-deserty')}}">Десерты</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="{{url('/pages/korporativnoe-pitanie')}}">Корпоративное питание</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <!--<i class="fa fa-angle-down"></i>-->
                                    <span class="dropdown-caption">Основное меню</span>
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{url('/catalog/zavtraki')}}">Завтраки</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/catalog/salaty-i-zakuski')}}">Салаты и закуски</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/catalog/supy')}}">Супы</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/catalog/kish-i-sendvichi')}}">Киш и сэндвичи</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/catalog/goryachie-blyuda/')}}">Горячее</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/catalog/garniry')}}">Гарниры</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/catalog/vypechka')}}">Выпечка</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/catalog/deserty')}}">Десерты</a>
                                        </li>
                                        <!--<li>
                                            <a href="{{url('/catalog/sousy-i-topingi')}}">Соусы и топинги</a>
                                        </li>-->
                                        <li>
                                            <a href="{{url('/catalog/napitki')}}">Напитки</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <span class="dropdown-caption">Кафе</span>
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{url("/pages/restaurants/aeroport")}}">м. Аэропорт</a>
                                        </li>
                                        <li>
                                            <a href="{{url("/pages/restaurants/kr-vorota")}}">м. Красные ворота</a>
                                        </li>
                                        <li>
                                            <a href="{{url("/pages/restaurants/technopark")}}">м. Технопарк</a>
                                        </li>
                                        <li>
                                            <a href="{{url("/pages/restaurants/trubnaya")}}">м. Трубная</a>
                                        </li>
                                        <li>
                                            <a href="{{url("/pages/restaurants/park-kultury")}}">м. Парк культуры</a>
                                        </li>
                                        <li>
                                            <a href="{{url("/pages/restaurants/taganskaya")}}">м. Таганская</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a  class="singlea" title="Акции" href="{{url('/pages/actions')}}">Акции</a>
                                </li>

                                {{--<li class="dropdown">
                                    <span  class="dropdown-caption">Контакты</span>
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{url('/pages/partneram')}}">Партнёрам</a>
                                            <a href="{{url('/pages/postavshchikam')}}">Поставщикам</a>
                                            <a href="{{url('/pages/franchayzing')}}">Франчайзинг</a>
                                            <a href="{{url('/pages/arendodatelyam')}}">Арендодателям</a>
                                        </li>
                                    </ul>
                                </li>--}}

                                <li class="dropdown">
                                    <span  class="dropdown-caption">О нас</span>
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{url('/pages/kontakty')}}">Контакты</a>
                                            {{--<a href="{{url('/pages/oferta')}}">Оферта</a>--}}
                                        </li>
                                    </ul>
                                </li>

                                <li class="dropdown droplogin">
                                    <div class="droplogin-text">
                                        <svg version="1.1" class="shopping-login-img" height="22px" width="22px"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000"
                                             xml:space="preserve">
                                        <g>
                                            <g transform="translate(0.000000,511.000000) scale(0.100000,-0.100000)">
                                                <path d="M4403.9,5003.7c-820.1-119.2-1631.4-436.2-2253.7-878.2c-354.8-250.1-918.9-814.2-1169-1169c-366.4-514.7-665.9-1203.9-814.2-1875.7c-55.2-253-64-378-66.9-959.6c0-593.2,8.7-703.7,66.9-985.8C542-2612.3,1775-4008.2,3432.6-4560.7c599.1-197.7,805.5-229.7,1552.9-229.7c729.9-2.9,884.1,17.5,1454,191.9c1724.4,523.4,3006.9,1933.8,3390.7,3733.9c61.1,279.2,69.8,395.5,69.8,971.3c0,712.5-20.4,875.3-194.8,1439.5c-523.4,1718.6-1933.8,3004-3719.3,3390.7c-247.2,52.3-410,66.9-898.6,72.7C4758.6,5012.5,4453.3,5009.6,4403.9,5003.7z M5857.9,4352.3c412.9-87.2,674.7-180.3,1076-378c828.8-410,1523.8-1105,1933.8-1933.8c337.3-677.6,444.9-1151.6,447.8-1919.3c2.9-759-113.4-1265-439.1-1928l-162.8-325.7L8574-2001.7c-293.7,276.3-948,625.2-1686.6,901.5l-366.4,139.6l-203.6-171.6c-264.6-221-474-346.1-741.5-442c-186.1-66.9-255.9-75.6-575.8-75.6s-389.7,8.7-575.8,75.6c-267.5,96-476.9,221-741.5,442l-203.6,171.6l-366.4-139.6c-738.6-276.3-1392.9-625.2-1689.6-904.4l-139.6-130.9l-159.9,328.6c-642.7,1320.3-599.1,2812,125,4074.1c354.8,625.2,965.5,1235.9,1590.7,1590.7c471.1,270.4,1078.9,476.9,1613.9,549.6C4784.8,4451.2,5517.6,4422.1,5857.9,4352.3z"/>
                                                <path d="M4592.9,3258.9c-287.9-61.1-628.1-241.4-863.7-459.5C2993.5,2119,2859.7,1077.9,3368.6,39.8c503.1-1026.5,1398.8-1494.7,2175.2-1140C6151.6-823.9,6669.2-126,6867,685.3c81.4,328.6,84.3,805.5,11.6,1090.5C6596.5,2840.2,5613.6,3479.9,4592.9,3258.9z"/>
                                            </g>
                                        </g>
                                    </svg>
                                        <span >Личный кабинет</span>
                                    </div>
                                    <a href="{{route('cart')}}" title="Личный кабинет" class="droplogin-link">
                                        <svg version="1.1" class="shopping-login-img" height="22px" width="22px"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000"
                                             xml:space="preserve">
                                        <g>
                                            <g transform="translate(0.000000,511.000000) scale(0.100000,-0.100000)">
                                                <path d="M4403.9,5003.7c-820.1-119.2-1631.4-436.2-2253.7-878.2c-354.8-250.1-918.9-814.2-1169-1169c-366.4-514.7-665.9-1203.9-814.2-1875.7c-55.2-253-64-378-66.9-959.6c0-593.2,8.7-703.7,66.9-985.8C542-2612.3,1775-4008.2,3432.6-4560.7c599.1-197.7,805.5-229.7,1552.9-229.7c729.9-2.9,884.1,17.5,1454,191.9c1724.4,523.4,3006.9,1933.8,3390.7,3733.9c61.1,279.2,69.8,395.5,69.8,971.3c0,712.5-20.4,875.3-194.8,1439.5c-523.4,1718.6-1933.8,3004-3719.3,3390.7c-247.2,52.3-410,66.9-898.6,72.7C4758.6,5012.5,4453.3,5009.6,4403.9,5003.7z M5857.9,4352.3c412.9-87.2,674.7-180.3,1076-378c828.8-410,1523.8-1105,1933.8-1933.8c337.3-677.6,444.9-1151.6,447.8-1919.3c2.9-759-113.4-1265-439.1-1928l-162.8-325.7L8574-2001.7c-293.7,276.3-948,625.2-1686.6,901.5l-366.4,139.6l-203.6-171.6c-264.6-221-474-346.1-741.5-442c-186.1-66.9-255.9-75.6-575.8-75.6s-389.7,8.7-575.8,75.6c-267.5,96-476.9,221-741.5,442l-203.6,171.6l-366.4-139.6c-738.6-276.3-1392.9-625.2-1689.6-904.4l-139.6-130.9l-159.9,328.6c-642.7,1320.3-599.1,2812,125,4074.1c354.8,625.2,965.5,1235.9,1590.7,1590.7c471.1,270.4,1078.9,476.9,1613.9,549.6C4784.8,4451.2,5517.6,4422.1,5857.9,4352.3z"/>
                                                <path d="M4592.9,3258.9c-287.9-61.1-628.1-241.4-863.7-459.5C2993.5,2119,2859.7,1077.9,3368.6,39.8c503.1-1026.5,1398.8-1494.7,2175.2-1140C6151.6-823.9,6669.2-126,6867,685.3c81.4,328.6,84.3,805.5,11.6,1090.5C6596.5,2840.2,5613.6,3479.9,4592.9,3258.9z"/>
                                            </g>
                                        </g>
                                        </svg>
                                        <span class="droplogin-text">Личный кабинет</span>
                                    </a>
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">
                                        @if(Auth::check())
                                            <li class="menu_cart_total">
                                                <a href="{{route('cabinet')}}"><strong>Личный кабинет</strong></a>
                                            </li>
                                            <li class="menu_cart_total">
                                                <a href="{{route('logout.get')}}"><strong>Выход</strong></a>
                                            </li>
                                        @else
                                            <li class="menu_cart_total">
                                                <a href="{{route('register')}}"><strong>Регистрация</strong></a>
                                            </li>
                                            <li class="menu_cart_total">
                                                <a href="{{route('login')}}"><strong>Вход</strong></a>
                                            </li>
                                        @endif
                                    </ul>
                                </li>
                                <li class="dropdown dropcart">
                                    <div class="shopping-cart-mobile-uppertext1">заказ за 48ч</div>
                                    <div class="shopping-cart-mobile-uppertext2">от 4000₽</div>
                                    <div class="shopping-cart-mobile-undertext">0₽</div>
                                    <div class="dropcart-text">
                                        <svg version="1.1" id="Capa_1" class="shopping-cart-img"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             width="20px" height="20px" viewBox="0 0 510 510"
                                             style="enable-background:new 0 0 510 510;" xml:space="preserve">
                                            <g>
                                                <g id="shopping-cart-path">
                                                    <path d="M153,408c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51S181.05,408,153,408z M0,0v51h51l91.8,193.8L107.1,306
                                                        c-2.55,7.65-5.1,17.85-5.1,25.5c0,28.05,22.95,51,51,51h306v-51H163.2c-2.55,0-5.1-2.55-5.1-5.1v-2.551l22.95-43.35h188.7
                                                        c20.4,0,35.7-10.2,43.35-25.5L504.9,89.25c5.1-5.1,5.1-7.65,5.1-12.75c0-15.3-10.2-25.5-25.5-25.5H107.1L84.15,0H0z M408,408
                                                        c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51S436.05,408,408,408z"/>
                                                </g>
                                            </g>
                                        </svg>
                                        <span >КОРЗИНА</span>
                                        <span class="badge badge-red cart-items-badge">0</span>
                                    </div>
                                    <a title="Корзина"  class="dropcart-link" href="#info" data-toggle="modal" data-target="#modal-info">
                                        <svg version="1.1" id="Capa_1" class="shopping-cart-img"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             width="20px" height="20px" viewBox="0 0 510 510"
                                             style="enable-background:new 0 0 510 510;" xml:space="preserve">
                                            <g>
                                                <g id="shopping-cart-path">
                                                    <path d="M153,408c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51S181.05,408,153,408z M0,0v51h51l91.8,193.8L107.1,306
                                                        c-2.55,7.65-5.1,17.85-5.1,25.5c0,28.05,22.95,51,51,51h306v-51H163.2c-2.55,0-5.1-2.55-5.1-5.1v-2.551l22.95-43.35h188.7
                                                        c20.4,0,35.7-10.2,43.35-25.5L504.9,89.25c5.1-5.1,5.1-7.65,5.1-12.75c0-15.3-10.2-25.5-25.5-25.5H107.1L84.15,0H0z M408,408
                                                        c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51S436.05,408,408,408z"/>
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="badge badge-red cart-items-badge">0</span>
                                    </a>
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">
                                        <li class="menu_cart_total">
                                            <a href="#info" data-toggle="modal" data-target="#modal-info"><strong>Итого: <span class="cart-items-summ">0</span> ₽</strong><span class="visible-xs pull-right menu_cart_total_cart">Перейти к корзине</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown drophelp">
                                    <span  class="dropdown-shopping">
                                        <svg version="1.1" class="shopping-question-img" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 473.733 473.733" style="enable-background:new 0 0 473.733 473.733;" xml:space="preserve" height="21px" width="21px">
                                            <g>
                                                <g>
                                                    <path d="M231.2,336.033c-9.35,0-17,7.65-17,17v11.333c0,9.35,7.65,17,17,17s17-7.65,17-17v-11.333
                                                        C248.2,343.683,240.55,336.033,231.2,336.033z"/>
                                                    <path d="M236.867,473.733c130.617,0,236.867-106.25,236.867-236.867S367.483,0,236.867,0S0,106.25,0,236.867
                                                        S106.25,473.733,236.867,473.733z M236.867,34c111.917,0,202.867,90.95,202.867,202.867s-90.95,202.867-202.867,202.867
                                                        S34,348.783,34,236.867S124.95,34,236.867,34z"/>
                                                    <path d="M163.2,194.367C163.483,194.367,163.483,194.367,163.2,194.367c9.35,0,17-7.083,17-16.433c0,0,0.283-13.6,7.083-26.917
                                                        c8.5-17,23.517-25.5,45.617-25.5c20.683,0,35.983,5.667,44.483,16.717c7.083,9.067,9.067,21.533,5.667,35.133
                                                        c-4.25,16.717-18.7,31.167-32.583,45.333c-17,17.567-34.85,35.417-34.85,59.5c0,9.35,7.65,17,17,17s17-7.65,17-17
                                                        c0-10.2,12.183-22.667,25.217-35.7c16.15-16.433,34.567-35.133,41.083-60.633c6.233-23.517,1.983-47.033-11.617-64.317
                                                        c-10.483-13.6-31.45-30.033-71.117-30.033c-44.483,0-65.733,23.8-75.933,44.2c-10.2,20.4-10.767,39.95-10.767,42.217
                                                        C146.483,187,153.85,194.367,163.2,194.367z"/>
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="drophelp-text">Условия заказа</span>
                                    </span>
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">
                                        <h4>УСЛОВИЯ</h4>
                                        <span>Минимальная сумма заказа 4000р.</span>
                                        <span>Оформление заказа не позднее 48 часов.</span>
                                        <span>Заказ оформленный после 15:00, примается в работу в 10:00 следующего дня.</span>
                                        <span>Зона доставки — в пределах МКАД</span>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                        <div class="social-link hidden-big-desktop">
                            <ul class="social-lists clear-list-style">
                                <!--<li>
                                    <a href="#!">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#!">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>-->
                                <li>
                                    <a href="https://www.instagram.com/dvakrendelya/" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @if(isset($slider))
                    @include('themes.krendels.components.slider')
                @endif
            </div>
        </div>
    </header>
    <!-- End Header-->