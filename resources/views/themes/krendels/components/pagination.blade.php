@if ($paginator->lastPage() > 1)
    <div class="page-prev">
        <a href="{{ $paginator->url(1) }}">
            <i class="fa fa-long-arrow-left"></i>
            <span>Пред.</span>
        </a>
    </div>
    <div class="pagination-wrapper">
        <ul class="au-pagination">
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endfor
        </ul>
    </div>
    <div class="page-next">
        <a href="{{ $paginator->url($paginator->currentPage()+1) }}" >
            <span>След.</span>
            <i class="fa fa-long-arrow-right"></i>
        </a>
    </div>
@endif