<footer>
    <div class="footer" style="background: url('/front/themes/krendels/img/bg-footer.jpg') center center no-repeat;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer-intro p-t-100 p-b-100">
                        <div class="footer-intro-logo m-b-15">
                            <!--<span>2 кренделя</span>-->
                            <img src="/front/themes/krendels/img/2kr_dark.png" width="200" alt="2 кренделя">
                        </div>
                        <div class="footer-intro-content">
                            <p>
                                Сеть современных кулинарий-кафе, где органично сочетаются концепции городского кафе, домашней кухни и кондитерской.
                            </p>
                            <p>{{--Общество с ограниченной ответственностью "Олимп"
                                Юридический /Фактический адрес:
                                РФ, 129090, г. Москва, Олимпийский пр-т, д. 16, стр. 2
                                ОГРН 5147746415120 ИНН/КПП 7702849863 / 770201001--}}
                                    email:&nbsp;zakaz@2krendelya.ru</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="footer-contact p-t-100 p-b-45">
                        <div class="footer-contact-inner" style="width: auto !important;">
                           {{-- <div class="footer-contact-form m-b-50">
                                <div class="footer-contact-label text-uppercase m-b-15">
                                    <h4>Подпишитесь на рассылку</h4>
                                </div>
                                <div class="footer-contact-input">
                                    <input class="au-input" placeholder="Ваш Email" />
                                    <a href="#!">
                                        <i class="fa fa-location-arrow"></i>
                                    </a>
                                </div>
                            </div>--}}
                            <div class="footer-contact-social">
                                <div class="footer-contact-label text-uppercase">
                                    <h4>Мы в соцсетях:</h4>
                                </div>
                                <ul class="footer-social-list clear-list-style">
                                    <li>
                                        <a href="https://www.instagram.com/dvakrendelya/" target="_blank">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                    <!--<li>
                                        <a href="#!">
                                            <i class="fa fa-vk"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#!">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>-->
                                    <li>
                                        <a href="https://www.facebook.com/dvakrendelya/" target="_blank">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer-->


<!-- Javascript-->
{{--<script src="/front/themes/krendels/vendor/jquery/dist/jquery.min.js"></script>--}}
<script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>

<script src="/front/themes/krendels/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/front/themes/krendels/vendor/headroom.js/dist/headroom.min.js"></script>
<script src="/front/themes/krendels/vendor/headroom.js/dist/jQuery.headroom.min.js"></script>
<!-- Preloader-->
{{--<script src="/front/themes/krendels/vendor/royal-preloader/royal_preloader.min.js"></script>--}}
<script src="/front/themes/krendels/js/general-function.js?<?php echo time(); ?>"></script>
<script>
    // (function ($) {
    //     "use strict";
    //     Royal_Preloader.config({
    //         mode: 'logo',
    //         logo: '/front/themes/krendels/img/2kr_dark.png',
    //         timeout: 1,
    //         showInfo: false,
    //         opacity: 1,
    //         background: ['#FFFFFF']
    //     });
    // })(jQuery);
</script>
<!-- Revo Slider Script-->
<script src="/front/themes/krendels/vendor/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/front/themes/krendels/vendor/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/front/themes/krendels/js/revo-function.min.js"></script>
<!-- Select and Date time picker-->
<script src="/front/themes/krendels/vendor/jquery-nice-select/js/jquery.nice-select.js"></script>
<script src="/front/themes/krendels/js/nice-select-function.min.js"></script>
<script src="/front/themes/krendels/vendor/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>
<script src="/front/themes/krendels/js/datetimepicker-function.js"></script>
<!-- Owl carousel-->
<script src="/front/themes/krendels/vendor/owl.carousel/dist/owl.carousel.min.js"></script>
<script src="/front/themes/krendels/js/owl-function.min.js"></script>
<script src="/front/themes/krendels/vendor/matchHeight/dist/jquery.matchHeight-min.js"></script>
<script src="/front/themes/krendels/js/matchHeight-function.min.js"></script>
<script src="/front/themes/krendels/js/jquery-ui.min.js"></script>


<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script src="/front/themes/krendels/js/cartcontrols.js?<?php echo time(); ?>"></script>
<script>

</script>
@yield('scripts')
{!! \App\Models\Settings::first()->code !!}
{{--<script src="{{ mix('/mix.js') }}"></script>--}}
</body>

</html>