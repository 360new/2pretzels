<section class="heading-page heading-menu-standard-page">
    <div class="heading-page-wrapper" style="background: url('{{$background}}') top center no-repeat;">
        <div class="container">
            <div class="heading-page-inner text-center">
                <div class="heading-page-title text-uppercase">
                    <h2>{{$title}}</h2>
                </div>
                @if(isset($need_breadcrumbs))
                    <div class="heading-page-breadcrumb">
                        <ul class="clear-list-style">
                            <li>
                                <a href="/">Главная</a>
                            </li>
                            @if(isset($elements))
                                @foreach($elements as $parent)
                                    <li>
                                        <a href="{{$parent['url']}}">{{$parent['caption']}}</a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>